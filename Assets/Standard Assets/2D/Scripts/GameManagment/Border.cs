﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Border : MonoBehaviour
{
    public bool fall = false;
    public bool fall2 = false;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1"))
        {
            GameData.roundb += 1;
            fall = true;
            fall2 = true;
        }
       else if (col.CompareTag("Player2"))
        {
            GameData.rounda += 1;
            fall = true;
            fall2 = true;
        }

    }
}
