﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerController : MonoBehaviour
{
    private PlayerCinematic jojo;
    private PlayerCinematic dio;
    private StandPower power1;
    private StandPower power2;
    protected Camera maincamera;
    protected Sounds sounds;
    bool camerafollow = false;
    void Start()
    {
        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        jojo = GameObject.FindGameObjectWithTag("Player1Cinematic").GetComponent<PlayerCinematic>();
        dio = GameObject.FindGameObjectWithTag("Player2Cinematic").GetComponent<PlayerCinematic>();
        power1 = jojo.GetComponentInChildren<StandPower>();
        power2 = dio.GetComponentInChildren<StandPower>();
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();

        maincamera.transform.position = new Vector3(-14f, 26.71f, -10);
        StartCoroutine("TrailerStart");
    }

    public IEnumerator TrailerStart()
    {
        yield return new WaitForSeconds(2f);

        //pan jojo
        while (maincamera.transform.position.x <- 13.5f)
        {
            maincamera.transform.position += new Vector3(.005f, 0,0);
            yield return new WaitForSeconds(.01f);
        }
        // pan dio

        maincamera.transform.position = new Vector3(-5.73f, 26.71f, -10);
        while (maincamera.transform.position.x > -5.9f)
        {
            maincamera.transform.position -= new Vector3(.005f, 0, 0);
            yield return new WaitForSeconds(.01f);
        }
        //point
        dio.StartCoroutine("Point");
        sounds.sound1();
        dio.StartCoroutine("Talk", 1);
        while (maincamera.transform.position.x > -6.5f)
        {
            maincamera.transform.position -= new Vector3(.005f, 0, 0);
            yield return new WaitForSeconds(.01f);
        }
        dio.StartCoroutine("Talk", .9f);
        yield return new WaitForSeconds(.2f);
        maincamera.transform.position += new Vector3(.55f, 0, 0);
        maincamera.orthographicSize = .5f;
        yield return new WaitForSeconds(1.1f);
        //jojo zoom
        maincamera.orthographicSize = 1;
        maincamera.transform.position = new Vector3(-14.14f, 26.71f, -10);
        sounds.sound2();
        jojo.StartCoroutine("Talk", 1f);
        while (maincamera.orthographicSize > .85f)
        {
            maincamera.orthographicSize -= .002f;
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(.2f);
        jojo.walking = true;
        yield return new WaitForSeconds(1.1f);
        sounds.sound3();
        yield return new WaitForSeconds(.2f);
        jojo.walking = false;
        jojo.StartCoroutine("Charging");
        yield return new WaitForSeconds(.1f);
        maincamera.orthographicSize = .5f;
        maincamera.transform.position = new Vector3(-12.47f, 26.71f, -10);
        yield return new WaitForSeconds(.5f);
        power1.transform.localPosition=new Vector3(0.055f, 0.015f);
        jojo.StartCoroutine("Talk", .3f);
        yield return new WaitForSeconds(1f);
        //dio monologue
        power2.transform.localPosition = new Vector3(999f,999f);
        maincamera.orthographicSize = 1f;
        maincamera.transform.position = new Vector3(-5.98f, 26.645f, -10);
        dio.StartCoroutine("Talk", 19f);
        yield return new WaitForSeconds(.2f);
        dio.StartCoroutine("Stance");
        while (maincamera.transform.position.y < 26.8f)
        {
            maincamera.transform.position += new Vector3(0, .0004f, 0);
            yield return new WaitForSeconds(.01f);
        }
        //joseph
        maincamera.orthographicSize = .5f;
        maincamera.transform.position = new Vector3(-1.872f, 7.79f, -10);
        while (maincamera.orthographicSize > .35f)
        {
            maincamera.orthographicSize -= .0004f;
            yield return new WaitForSeconds(.01f);
        }
        maincamera.orthographicSize = 1f;
        maincamera.transform.position = new Vector3(-5.98f, 26.71f, -10);
        yield return new WaitForSeconds(5.3f);
        //jojo continue
        jojo.transform.position = new Vector3(-16f, 26.65f, 0);
        jojo.walking = true;
        yield return new WaitForSeconds(.2f);
        maincamera.transform.position = new Vector3(-12.47f, 26.71f, -10);
        camerafollow = true;
        yield return new WaitForSeconds(3.5f);
        camerafollow = false;
        jojo.walking = false;
        maincamera.transform.position = new Vector3(-5.98f, 26.71f, -10);
        power2.transform.localPosition = new Vector3(0f, 0.038f);
        yield return new WaitForSeconds(.5f);
        dio.StartCoroutine("Talk", 3f);
        yield return new WaitForSeconds(2.5f);
        dio.walking = true;
        power2.transform.localPosition = new Vector3(999f, 999f);
        yield return new WaitForSeconds(.5f);
        jojo.transform.position = new Vector3(-14.1f, 26.65f, 0);
        maincamera.orthographicSize = 1.9f;
        maincamera.transform.position = new Vector3(-10.62f, 27.59f, -10);
        jojo.walking = true;
    }
    private void Update()
    {
        if(camerafollow)
        {
            maincamera.transform.position = new Vector3(jojo.transform.position.x, 26.71f, -10);
        }
    }

}
