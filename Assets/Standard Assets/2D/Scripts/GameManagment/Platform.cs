﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{

    public PlayerControl player;
    public float fallingUnder = 0.5f;
    public float jumpingOn = 0.5f;
    bool jumpingDown;
    bool jumpingUp;


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
    }

    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            GetComponent<BoxCollider2D>().enabled = false;
            jumpingDown = true;
        }

        if (jumpingDown == true)
        {
            if (transform.position.y - player.transform.position.y > fallingUnder) { GetComponent<BoxCollider2D>().enabled = true; jumpingDown = false; }
        }



        if (Input.GetButtonDown("Jump"))
        {
            GetComponent<BoxCollider2D>().enabled = false;
            jumpingUp = true;
        }


        if (jumpingUp == true)
        {
            if (player.transform.position.y - transform.position.y > jumpingOn) { GetComponent<BoxCollider2D>().enabled = true; jumpingUp = false; }
        }


    }




}