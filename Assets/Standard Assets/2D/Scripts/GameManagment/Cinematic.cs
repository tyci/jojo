﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
public class Cinematic : MonoBehaviour
{
    protected VideoPlayer video;
    protected Image image;
    protected AudioSource sounds;
    void Awake()
    {
        video = GetComponent<VideoPlayer>();
        image = GameObject.FindGameObjectWithTag("video").GetComponent<Image>();
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<AudioSource>();
        if (PlayerPrefs.GetInt("Cinematic")==0)
        {
            video.Play();
            image.gameObject.SetActive(true);
        }
       else
        {
            image.gameObject.SetActive(false);
            sounds.Play();
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && PlayerPrefs.GetInt("Cinematic") == 0)
        {
            video.Stop();
            image.gameObject.SetActive(false);
            PlayerPrefs.SetInt("Cinematic", 1);
            sounds.Play();
        }

        video.loopPointReached += CheckOver;
    }

    void CheckOver(VideoPlayer vp)
    {
        video.Stop();
        image.gameObject.SetActive(false);
        PlayerPrefs.SetInt("Cinematic", 1);
        sounds.Play();
    }



}
