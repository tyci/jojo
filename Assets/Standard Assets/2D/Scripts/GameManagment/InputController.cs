﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private PlayerControl player1;
    private PlayerControl player2;
    private PlayersAttack player1attack;

    private PlayersAttack player2attack;

    void Start()
    {
        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
        player1attack = player1.GetComponent<PlayersAttack>();
        player2attack = player2.GetComponent<PlayersAttack>();

        if (PlayerPrefs.GetString("Controls")== "Laptop")
        {
            if (!GameData.jojoAI)
            {
                player1.DashKey = KeyCode.G;
                player1attack.AttackButton = KeyCode.T;
                player1attack.GuardKey = KeyCode.U;
                player1attack.StandKey = KeyCode.Y;
            }
            if (!GameData.dioAI)
            {
                player2.JumpKey = KeyCode.RightBracket;
                player2.DashKey = KeyCode.Quote;
                player2attack.AttackButton = KeyCode.LeftBracket;
                player2attack.GuardKey = KeyCode.O;
                player2attack.StandKey = KeyCode.P;
            }
        }
        else
        {
            if (!GameData.jojoAI)
            {
                player1.DashKey = KeyCode.K;
                player1attack.AttackButton = KeyCode.I;
                player1attack.GuardKey = KeyCode.P;
                player1attack.StandKey = KeyCode.O;
            }

            if (!GameData.dioAI)
            {
                player2.JumpKey = KeyCode.Keypad0;
                player2.DashKey = KeyCode.Keypad1;
                player2attack.AttackButton = KeyCode.Keypad4;
                player2attack.GuardKey = KeyCode.Keypad6;
                player2attack.StandKey = KeyCode.Keypad5;
            }
        }

            


        }
    }



