﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    protected Transform CameraParent;
    protected Camera maincamera;
    protected PlayerControl Player1;
    protected PlayerControl Player2;
    protected Renderer Player1Renderer;
    protected Renderer Player2Renderer;
    float originalSize;
    float followNow;
    void Start()
    {
        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        CameraParent = GetComponentInParent<Transform>();
        Player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        Player1Renderer = Player1.GetComponent<Renderer>();
        Player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
        Player2Renderer = Player2.GetComponent<Renderer>();


        originalSize = maincamera.orthographicSize;
    }

    void Update()
    {


        //location
        Vector3 pos1 = Player1.transform.position;
        Vector3 pos2 = Player2.transform.position;
        Vector3 pos = (pos1 + pos2) / 2f;
        CameraParent.transform.position = new Vector3(pos.x, pos.y, -10);

        //size
        Bounds bounds = Player1Renderer.bounds;
        bounds.Encapsulate(Player2Renderer.bounds);
        
        float sizeX = Mathf.Abs(bounds.max.x- bounds.min.x);
        float sizeY = Mathf.Abs(bounds.max.y - bounds.min.y);
        float size = (sizeX + sizeY) / 2;
        size *= 0.7f;

        if (size > originalSize)
        {
            size = originalSize;
        }
        if (size < 1.4f)
        {
            size = 1.4f;
        }

       maincamera.orthographicSize = Calculate(size, maincamera.orthographicSize);
    }

    float Calculate(float size,float Camera)
    {
        return Mathf.SmoothStep(Camera, size, Time.deltaTime * 10);
    }
}
