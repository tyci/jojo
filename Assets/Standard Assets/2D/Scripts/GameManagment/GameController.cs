﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    private PlayerControl player1;
    private PlayerControl player2;
    private Border border;
    private HUD hud;
    public Text Rounds;
    protected Sounds sounds;
    //close up
    protected Transform CameraParent;
    protected Camera maincamera;
    protected CameraFollow camerafollow;
    private PlayerCinematic player1cinematic;
    private PlayerCinematic player2cinematic;
    private GameObject dioAI;
    private GameObject jojoAI;
    private PauseMenu pauseMenu;
    bool intro1 = false;
    bool intro2 = false;
    bool roundOver = false;
    void Awake()
    {
        dioAI = GameObject.FindGameObjectWithTag("AI");
        if (GameData.dioAI)
        {
            player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
            player2.gameObject.SetActive(false);
            dioAI.tag = "Player2";
            dioAI.transform.position = player2.transform.position;
        }
        else
        {
            dioAI.gameObject.SetActive(false);
        }
        jojoAI = GameObject.FindGameObjectWithTag("AI2");
        if (GameData.jojoAI)
        {
            
            player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
            player1.gameObject.SetActive(false);
            jojoAI.tag = "Player1";
            jojoAI.transform.position = player1.transform.position;
        }
        else
        {
            jojoAI.gameObject.SetActive(false);
        }
        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
        hud = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HUD>();
        border= GameObject.FindGameObjectWithTag("Border").GetComponent<Border>();

      
    }

    private void Start()
    {  
        //******************************
       //ENABLE BEFORE BUILDING
        StartCoroutine("RoundStart", GameData.rounda + GameData.roundb + 1);
        //********************************

        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        CameraParent = maincamera.GetComponentInParent<Transform>();
        camerafollow = maincamera.GetComponentInParent<CameraFollow>();
        player1cinematic = GameObject.FindGameObjectWithTag("Player1Cinematic").GetComponent<PlayerCinematic>();
        player2cinematic = GameObject.FindGameObjectWithTag("Player2Cinematic").GetComponent<PlayerCinematic>();

        pauseMenu = GameObject.FindGameObjectWithTag("Pause").GetComponentInChildren<PauseMenu>();
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        hud.t = GameData.timer;

    }
    void Update()
    {
        //hp check
        if (player1.curHealth <= 0 && GameData.roundb==0 &&!roundOver)
        {
            roundOver = true;
            GameData.roundb = 1;
            StartCoroutine("RoundEnd");
        }
        if (player2.curHealth <= 0 && GameData.rounda == 0 && !roundOver)
        {
            roundOver = true;
            GameData.rounda = 1;
            StartCoroutine("RoundEnd");
        }
        if (player1.curHealth <= 0 && GameData.roundb == 1 && !roundOver)
        {
            roundOver = true;
            GameData.roundb = 2;
            StartCoroutine("MatchEnd");
        }
        if (player2.curHealth <= 0 && GameData.rounda == 1 && !roundOver)
        {
            roundOver = true;
            GameData.rounda = 2;
            StartCoroutine("MatchEnd");
        }
        //border
        if (border.fall && GameData.rounda < 2 && GameData.roundb < 2 && !roundOver)
        {
            roundOver = true;
            StartCoroutine("RoundEnd");
            border.fall = false;
        }
        else if (border.fall && (GameData.rounda == 2 || GameData.roundb == 2) && !roundOver)
        {
            roundOver = true;
            StartCoroutine("MatchEnd");
            border.fall = false;
        }
        //timer
        if (hud.t < 0 && hud.t>-1 && !roundOver)
        {
            roundOver = true;
            if (player1.curHealth > player2.curHealth)
                GameData.rounda += 1;
            else if (player1.curHealth < player2.curHealth)
                GameData.roundb += 1;

            hud.Timer.text = "0";
            hud.t = -1;

            if (GameData.roundb<2 && GameData.rounda<2)
                StartCoroutine("RoundEnd");
            else
                StartCoroutine("MatchEnd");
        }
        //mp control
        if (player1.curmana <= 0)
        {
            player1.curmana = 0;
        }
        if (player2.curmana <= 0)
        {
            player2.curmana = 0;
        }
        //hp control
        if (player1.curHealth <= 0)
        {
            player1.curHealth = 0;
        }
        if (player2.curHealth <= 0)
        {
            player2.curHealth = 0;
        }

        //camera follow
        if (intro1)
            CameraParent.transform.position = new Vector3(player1cinematic.transform.position.x, player1cinematic.transform.position.y, -10);
        if (intro2)
            CameraParent.transform.position = new Vector3(player2cinematic.transform.position.x, player2cinematic.transform.position.y, -10);

    }

    public IEnumerator RoundStart(int i)
    {
        MonoBehaviour[] scripts = player1.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
        MonoBehaviour[] scripts2 = player2.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts2)
        {
            script.enabled = false;
        }
        yield return new WaitForSeconds(.1f);
        Rounds.gameObject.SetActive(true);
        if ((GameData.rounda + GameData.roundb) == 0)
        {
            float temp = maincamera.orthographicSize;
            Vector3 pos = CameraParent.transform.position;

            player2cinematic.Intro();
            camerafollow.enabled = false;
            maincamera.orthographicSize = 1;
            intro2 = true;
            player2.BroadcastMessage("sound12");
            yield return new WaitForSeconds(1.2f);

            player1cinematic.Intro();
            intro2 = false;
            intro1 = true;
            player1.BroadcastMessage("sound8");
            yield return new WaitForSeconds(1.2f);

            intro1 = false;
            camerafollow.enabled = true;
            CameraParent.transform.position = pos;
            maincamera.orthographicSize = temp;
        }
        sounds.SendMessageUpwards("sound16");
        yield return new WaitForSeconds(.1f);
        Rounds.text = "Round " + i;
        yield return new WaitForSeconds(1.35f);
        sounds.SendMessageUpwards("sound17");
        yield return new WaitForSeconds(.2f);
        Rounds.text = "Fight! ";
        yield return new WaitForSeconds(.7f);
        Rounds.text = "";
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = true;
        }
        foreach (MonoBehaviour script in scripts2)
        {
            script.enabled = true;
        }
        Rounds.gameObject.SetActive(false);
    }
    public IEnumerator RoundEnd()
    {
        Rounds.gameObject.SetActive(true);
        if (!border.fall2)
        {
            Time.timeScale = .3f;
            yield return new WaitForSeconds(.3f);
        }
        sounds.SendMessageUpwards("sound18");
        yield return new WaitForSeconds(.1f);
        Time.timeScale = 0.01f;
        Rounds.text = "Round Over!";
        yield return new WaitForSeconds(.02f);

        Rounds.text = "";
        yield return new WaitForSeconds(.01f);
        Time.timeScale = 1f;
    
        Rounds.gameObject.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public IEnumerator MatchEnd()
    {
        Rounds.gameObject.SetActive(true);
        if (!border.fall2)
        {
            Time.timeScale = .3f;
            yield return new WaitForSeconds(.4f);
        }
        Time.timeScale = 0.01f;
        if ( GameData.roundb == 2)
            player1.BroadcastMessage("sound9");
        else
            player2.BroadcastMessage("sound11");
        yield return new WaitForSeconds(.01f);

        Rounds.text = "K.O.";
        yield return new WaitForSeconds(.02f);

        if (GameData.roundb == 2)
            Rounds.text = "Dio wins!";
        else
            Rounds.text = "Jotaro wins!";
        yield return new WaitForSeconds(.02f);

        Rounds.text = "";
        player1.gameObject.SetActive(false);
        player2.gameObject.SetActive(false);
        Time.timeScale = 1f;
        camerafollow.enabled = false;
        maincamera.orthographicSize = 1;
        if (GameData.roundb == 2)
        {
            CameraParent.transform.position = new Vector3(player2cinematic.transform.position.x, player2cinematic.transform.position.y, -10);
            player2cinematic.BroadcastMessage("sound9");
            player2cinematic.Special1();
        }
        else
        {
             CameraParent.transform.position = new Vector3(player1cinematic.transform.position.x, player1cinematic.transform.position.y, -10);
            player1cinematic.BroadcastMessage("sound2");
            player1cinematic.Special1();
        }
        yield return new WaitForSeconds(1.8f);
        pauseMenu.cinematic = true;
        Rounds.gameObject.SetActive(false);
        pauseMenu.RestartScreen();
    }


}
