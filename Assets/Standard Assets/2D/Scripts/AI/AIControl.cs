﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControl : MonoBehaviour
{
    protected PlayerControl player1;
    protected PlayerControl ai;
    protected PlayersAttack aiAttack;
    protected Animator anim;
    protected Rigidbody2D rb2d;
    protected Sounds SoundController;
    protected GroundCheck aiground;
    [SerializeField] public float speed = 50f;
    public bool walking = false;
    public bool facingLeft = false;
    public bool attacking = false;
    public bool stop = false;
    public bool charging = false;
    public bool jumping = false;
    public bool timestop = false;
    protected virtual void Start()
    {
        ai = GetComponent<PlayerControl>();
        anim = GetComponent<Animator>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        SoundController = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        aiground = GetComponentInChildren<GroundCheck>();

        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        aiAttack = GetComponent<DioAttack>();
    }


    protected virtual void Update()
    {
        //behaviour
        if (!attacking && !stop &&!charging && ai.grounded)    
        {

            //dash
            if ((transform.position.x - player1.transform.position.x) > 3f && ((transform.position.x - player1.transform.position.x) < 3.5f || timestop) && !ai.doubletapL && !ai.doubletapR)
            {
                ai.doubletapL = true;
                ai.StartCoroutine("DashLeft");
            }
            else if ((transform.position.x - player1.transform.position.x) < -3f && ((transform.position.x - player1.transform.position.x) > -3.5f || timestop) && !ai.doubletapL && !ai.doubletapR)
            {
                ai.doubletapR = true;
                ai.StartCoroutine("DashRight");
            }
            //move to player
            else if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 1.2f && Mathf.Abs(transform.position.x - player1.transform.position.x) < 3f)
            {
                walking = true;
            }
            //stop
            else if (walking && Mathf.Abs(transform.position.x - player1.transform.position.x) < 1.2f)
                StartCoroutine("StopWalking");
            //stand && attack
            else if (!walking && Mathf.Abs(transform.position.x - player1.transform.position.x) < 1.2f)
                Attack();


            //jump 
            if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 1.2f && player1.transform.position.y>transform.position.y+1 &&!jumping)
            {
                jumping = true;
                ai.StartCoroutine("PreJump");
                StartCoroutine("StopJump");
            }
            else if (player1.transform.position.y+.5f < transform.position.y && !jumping)
            {
                aiground.StartCoroutine("Drop");
            }


          
             


            //left right
            if (transform.position.x > player1.transform.position.x)
                facingLeft = true;
            else
                facingLeft = false;

            if (facingLeft)
            {
                transform.localScale = new Vector3(-2, 2, 2);
            }
            else
            {
                transform.localScale = new Vector3(2, 2, 2);
            }

            if (walking && !facingLeft)
                rb2d.AddForce(Vector2.right * speed);
            else if (walking && facingLeft)
                rb2d.AddForce(Vector2.left * speed);

        }

        //isDamaged
        if (anim.GetInteger("Damage") != 0)
            {
            attacking = false;
            stop = true;
            charging = false;
            aiAttack.standpower.charging = false;
            StopAllCoroutines();
            }
            else
                stop = false;


        if (charging)
        {

            if (ai.curmana < 75 || (ai.curHealth <= 10 && aiAttack.Awakening))
            {
                ai.curmana += (Time.deltaTime * 28f);
                aiAttack.standpower.charging = true;
            }
            else
            {
                charging = false;
                attacking = false;
                ai.BroadcastMessage("stop");
                aiAttack.PreCharge = false;
                aiAttack.standpower.charging = false;
            }
            if (aiAttack.Awakening && ai.curHealth <= 10 && ai.curmana > 99)
            {
                aiAttack.Awakening = false;
                aiAttack.standOn = true;
                aiAttack.AwakeningTimer = 12f;

                aiAttack.StartCoroutine("Awaken");
                aiAttack.StartCoroutine("CloseUp");
                aiAttack.PreCharge = false;

                aiAttack.standpower.charging = false;
                charging = false;
                attacking = false;
                ai.BroadcastMessage("stop");
            }

        }
        

    }

    public virtual IEnumerator StopWalking()
    {
        yield return new WaitForSeconds(.1f);
        walking = false;
    }
    public virtual IEnumerator StopAttack(float i)
    {
        yield return new WaitForSeconds(i);
        attacking = false;
    }
    public virtual IEnumerator StopJump()
    {
        yield return new WaitForSeconds(.1f);
        jumping = false;
    }

    public virtual void Attack()
    {
        attacking = true;
        timestop = false;
        walking = false;


    }
    public virtual IEnumerator Combo()
        {

        if ((transform.position.x - player1.transform.position.x) > -1.2f && (transform.position.x - player1.transform.position.x) < 1.2f)
            {
                aiAttack.Attacking = true;
                aiAttack.attackTimer = 0.4f;
                int rInt = Random.Range(13, 16);
                SoundController.SendMessageUpwards("random", rInt);
                yield return new WaitForSeconds(.3f);
            }

            if ((transform.position.x - player1.transform.position.x) > -1.2f && (transform.position.x - player1.transform.position.x) < 1.2f)
            {
                aiAttack.combo1 = true;
                aiAttack.attackTimer2 = 0.4f;
                int rInt = Random.Range(13, 16);
                SoundController.SendMessageUpwards("random", rInt);
                aiAttack.StartCoroutine("SoundRandom", rInt);
                yield return new WaitForSeconds(.3f);
            }

            if ((transform.position.x - player1.transform.position.x) > -1.2f && (transform.position.x - player1.transform.position.x) < 1.2f)
            {
                aiAttack.combo2 = true;
                aiAttack.attackTimer3 = 1f;
                ai.BroadcastMessage("sound10");
                int rInt = Random.Range(13, 16);
                aiAttack.StartCoroutine("SoundRandom", rInt);
                yield return new WaitForSeconds(1f);
            }
            yield return new WaitForSeconds(.2f);
            attacking = false;
        }
    
    

   protected virtual void Charge()
    {
        attacking = true;
        charging = true;

        ai.BroadcastMessage("sound20");
        aiAttack.combo = false;
        aiAttack.attackTrigger.enabled = false;
        aiAttack.combo1 = false;
        aiAttack.combo2 = false;
        aiAttack.Attacking = false;
        aiAttack.PreCharge = true;

    }

   

   protected virtual void SmashFront()
    {
        aiAttack.attackTimer3 = .9f;
        aiAttack.frontattack = true;
        aiAttack.attackTrigger.size += new Vector2(.8f, 0);
        anim.SetBool("Combo2", true);
        if (!aiAttack.standOn)
            ai.curmana -= 10;
        SoundController.SendMessageUpwards("sound10");

    }
    protected virtual void SmashUp()
    {
        aiAttack.attackTimer3 = .9f;
        aiAttack.uppercut = true;
        aiAttack.attackTrigger.size += new Vector2(.8f, 0);
        anim.SetBool("Combo", true);
        if (!aiAttack.standOn)
            ai.curmana -= 10;
        SoundController.SendMessageUpwards("sound10");

    }
}
