﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JojoAI : AIControl
{
    protected JojoAttack aiAttack2;
    protected override void Start()
    {
        base.Start();
        player1 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
        aiAttack = GetComponent<JojoAttack>();
        aiAttack2 = GetComponent<JojoAttack>();
    }

    protected override void Update()
    {
        base.Update();
        //specials:
        if (!attacking && !stop && !charging && ai.grounded && !ai.doubletapL && !ai.doubletapR && !jumping)
        {
            if (walking && (Mathf.Abs(player1.rb2d.velocity.x)) > 3f && (Mathf.Floor(player1.transform.position.y) == Mathf.Floor(transform.position.y)) && ai.curmana>35
                && ( (facingLeft&&!player1.facingLeft) || (!facingLeft && player1.facingLeft)) )
            {
                YareYare();
                StartCoroutine("StopAttack", 1.1f);
            }


            //chargeattack
            if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && Mathf.Abs(transform.position.x - player1.transform.position.x) < 4f && ai.curmana > 40)
            {
                ChargeAttack();
                StartCoroutine("StopAttack", 1f);

            }
            else if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && ai.curmana < 40)
            {
                Charge();
            }
        }

    }

    public override void Attack()
    {
        base.Attack();

        int random = Random.Range(1, 6);
        if (random == 5 && ai.curmana > 45)
        {
            OraOra();
            StartCoroutine("StopAttack", 1.4f);
        }
        else if (random == 4 && ai.curmana > 20)
        {
            SmashUp();
            StartCoroutine("StopAttack", 1);
        }
        else if (random == 3 && ai.curmana > 20)
        {
            SmashFront();
            StartCoroutine("StopAttack", 1);
        }
        else
        {
            StartCoroutine("Combo");
        }
    }




    void OraOra()
    {
        attacking = true;

        aiAttack2.special2 = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.specialTimer = 1.2f;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        aiAttack2.attackTrigger.offset += new Vector2(1.3f, .3f);
        aiAttack2.attackTrigger.size += new Vector2(1, 0.6f);
        if (!aiAttack.standOn)
            ai.curmana -= 40;
        ai.BroadcastMessage("sound4");
        SoundController.SendMessageUpwards("sound10");

    }


    void ChargeAttack()
    {
        attacking = true;

        aiAttack2.special3 = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        aiAttack2.specialTimer = .9f;
        if (!aiAttack2.standOn)
            ai.curmana -= 35;
        aiAttack2.StartCoroutine("TriggerSize");
        ai.BroadcastMessage("sound7");
        SoundController.SendMessageUpwards("sound10");
        anim.SetBool("Special3", true);
    }

    void YareYare()
    {
        attacking = true;

        aiAttack2.special1 = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.specialTimer = 1f;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        ai.BroadcastMessage("sound2");
        if (!aiAttack2.standOn)
            ai.curmana -= 30;
        anim.SetBool("Special1", true);
    }
}
