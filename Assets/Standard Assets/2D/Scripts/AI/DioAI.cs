﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DioAI : AIControl
{
    protected DioAttack aiAttack2;
    protected override void Start()
    {
        base.Start();
        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        aiAttack2 = GetComponent<DioAttack>();
    }

    protected override void Update()
    {
        base.Update();
        //specials:
        if (!attacking && !stop && !charging && ai.grounded&& !ai.doubletapL && !ai.doubletapR && !timestop && !jumping)
                {
            //zawarudo
            if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && ai.curmana > 65)
            {
                Zawarudo();
                StartCoroutine("StopAttack", 2.5f);
            }
            //knife throw
            else if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && ai.curmana > 30)
            {
                KnifeThrow();
                StartCoroutine("StopAttack", 1.2f);
                StartCoroutine("KnifeThrow2");

            }
            else if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && ai.curmana > 25)
            {
                KnifeThrow();
                StartCoroutine("StopAttack", .7f);
            }
            else if (Mathf.Abs(transform.position.x - player1.transform.position.x) > 3.5f && ai.curmana < 25)
            {
                Charge();
            }
        }

    }

    public override  void Attack()
    {
        base.Attack();

        int random = Random.Range(1, 6);
        if (random == 5 && ai.curmana > 45)
        {
            MudaMuda();
            StartCoroutine("StopAttack", 1.4f);
        }
        else if (random == 4 && ai.curmana > 20)
        {
            SmashUp();
            StartCoroutine("StopAttack", 1);
        }
        else if (random == 3 && ai.curmana > 20)
        {
            SmashFront();
            StartCoroutine("StopAttack", 1);
        }
        else
        {
            StartCoroutine("Combo");
        }
    }

    void KnifeThrow()
    {
        attacking = true;

        aiAttack2.special3 = true;
        aiAttack2.special3b = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        aiAttack2.specialTimer = .6f;
        if (!aiAttack2.standOn)
            ai.curmana -= 15;
        ai.BroadcastMessage("sound7");

        anim.SetBool("Special3", true);
    }
    public IEnumerator KnifeThrow2()
    {
        yield return new WaitForSeconds(.3f);
        if (!aiAttack2.standOn)
            ai.curmana -= 10;
        anim.SetBool("Special3b", true);
        aiAttack2.knifeThrow.knives = true;
        aiAttack2.specialTimer = .75f;
        ai.BroadcastMessage("sound5");
    }

    void Zawarudo()
    {
        attacking = true;
        timestop = true;

        aiAttack2.special1 = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.specialTimer2 = 10f;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        //   audio.Play();
        ai.BroadcastMessage("sound1");
        if (!aiAttack2.standOn)
            ai.curmana -= 55;

    }

    void MudaMuda()
    {
        attacking = true;

        aiAttack2.special2 = true;
        aiAttack2.attackTrigger.enabled = false;
        aiAttack2.specialTimer = 1.2f;
        aiAttack2.Attacking = false;
        aiAttack2.combo1 = false;
        aiAttack2.combo2 = false;
        aiAttack2.attackTrigger.offset += new Vector2(1.3f, .3f);
        aiAttack2.attackTrigger.size += new Vector2(1, 0.6f);
        if (!aiAttack2.standOn)
            ai.curmana -= 40;
        ai.BroadcastMessage("sound3");
        SoundController.SendMessageUpwards("sound10");

    }
}
