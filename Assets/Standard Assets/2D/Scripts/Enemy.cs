﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int curHealth;
    public int maxHealth = 5;
    public Animator anim;
    public Vector3 moveDirection;
    private Rigidbody2D rb2d;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        curHealth = maxHealth;
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }
        if (curHealth <= 0)
        {
            Die();
        }
    }

    public void Damage(int dmg)
    {
        curHealth -= dmg;
       
    }

    void Die()
    {
        Destroy(gameObject);
    }

    public void Knockback(int direction)
    {

        if(direction==1)
            rb2d.AddForce(new Vector3(-300, 300, transform.position.z));
        else if (direction == 2)
            rb2d.AddForce(new Vector3(300, 300, transform.position.z));
        //stronger
        else if (direction == 3)
            rb2d.AddForce(new Vector3(-800, 450, transform.position.z));
        else if (direction == 4)
            rb2d.AddForce(new Vector3(800, 450, transform.position.z));
    } 


}
