﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour { 


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("Ground"))
        {

            col.SendMessageUpwards("Damage", 1);

            if (col.transform.position.x < transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 3);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 4);
            }

        }
    }



}
