﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{
    public GameObject SettingsUI;
    public GameObject MultiplayerUI;
    public GameObject SingleplayerUI;

    private void Start()
    {
        GameData.rounda = 0;
        GameData.roundb = 0;
    }

    public void Options()
    {
        OpenSettings();
    }
    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void OpenSettings()
    {
        SettingsUI.SetActive(true);
        MultiplayerUI.SetActive(false);
        SingleplayerUI.SetActive(false);
    }
    public void OpenMultiplayer()
    {
        MultiplayerUI.SetActive(true);
        SettingsUI.SetActive(false);
        SingleplayerUI.SetActive(false);
    }
    public void OpenSingleplayer()
    {
        SingleplayerUI.SetActive(true);
        SettingsUI.SetActive(false);
        MultiplayerUI.SetActive(false);
    }

    public void Cinematic()
    {
        MultiplayerUI.SetActive(false);
        SettingsUI.SetActive(false);
        SingleplayerUI.SetActive(false);
        PlayerPrefs.SetInt("Cinematic", 0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
