﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class HUD : MonoBehaviour
    {
    private PlayerControl player1;
    private PlayerControl player2;
    private PlayersAttack player1attack;
    private PlayersAttack player2attack;

    [SerializeField] public HealthBar health1;
    [SerializeField] public HealthBar health2;
    [SerializeField] public HealthBar mana1;
    [SerializeField] public HealthBar mana2;
    public TextMeshProUGUI Power;
    public TextMeshProUGUI Power2;
    public Text Timer;
    public SpriteRenderer counter1a;
    public SpriteRenderer counter1b;
    public SpriteRenderer counter2a;
    public SpriteRenderer counter2b;
    public float t=12f;


    void Start()
        {
        player1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
        player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
        player1attack = player1.GetComponent<PlayersAttack>();
        player2attack = player2.GetComponent<PlayersAttack>();
    }
    void Update()
    {
        //hp
        health1.SetSize(player1.curHealth * 0.05f);
        health2.SetSize(player2.curHealth * 0.05f);
        //mp
        mana1.SetSize(Mathf.Floor(player1.curmana) * 0.01f);
        mana2.SetSize(Mathf.Floor(player2.curmana) * 0.01f);
        //stand power
        if (player1.curHealth <= 10 && player1attack.Awakening)
        {
            Power.text = "STAND POWER AVAILABLE!";
        }
        else
            Power.text = "";
        if (player2.curHealth <= 10 && player2attack.Awakening)
            Power2.text = "STAND POWER AVAILABLE!";
        else
            Power2.text = "";
        //counter
        if (GameData.rounda == 1)
        {
            counter1a.color = Color.gray;
        }
        if (GameData.roundb == 1)
        {
            counter2a.color = Color.gray;
        }
        if (GameData.rounda == 2)
        {
            counter1b.color = Color.gray;
        }
        if (GameData.roundb == 2)
        {
            counter2b.color = Color.gray;
        }

        if (t > 0)
        {
            t -= Time.deltaTime;
            Timer.text = Mathf.Floor(t).ToString("R");
        }
        else if (t == -99)
            Timer.text = "∞";
    }



}
