﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SingleMenu : MonoBehaviour
{
    public GameObject SingleplayerUI;
    public TMPro.TMP_Dropdown dropdown;
    public TMPro.TMP_Dropdown dropdown2;
    public TMPro.TMP_Dropdown dropdown3;
    public TMPro.TextMeshProUGUI card1;
    public TMPro.TextMeshProUGUI card2;
    void Start()
    {
        int rInt = Random.Range(1, 3);
        GameData.map = rInt;
        int rInt2 = Random.Range(1, 4);
        GameData.music = rInt2;
        GameData.timer = 120;
        dropdown3.value = 4;
        GameData.dioAI = false;
        GameData.jojoAI  =false;
    }
    public void Single()
    {
        if (GameData.dioAI == false && GameData.jojoAI == false)
            GameData.dioAI = true;

        if (GameData.map == 1)
            SceneManager.LoadScene("DioCastle");
        else
            SceneManager.LoadScene("Clocktower");
    }
    public void Map()
    {
        if (dropdown.value == 2)
        {
            GameData.map = 2;
        }
        else if (dropdown.value == 1)
        {
            GameData.map = 1;
        }
        else if (dropdown.value == 0)
        {
            int rInt = Random.Range(1, 3);
            GameData.map = rInt;
        }
    }
    public void Music()
    {
        if (dropdown2.value == 3)
        {
            GameData.music = 3;
        }
        else if (dropdown2.value == 2)
        {
            GameData.music = 2;
        }
        else if (dropdown2.value == 1)
        {
            GameData.music = 1;
        }
        else if (dropdown2.value == 0)
        {
            int rInt = Random.Range(1, 4);
            GameData.music = rInt;
        }
    }
    public void Time()
    {
        if (dropdown3.value == 0)
        {
            GameData.timer = -99;
        }
        else if (dropdown3.value == 1)
        {
            GameData.timer = 30;
        }
        else if (dropdown3.value == 2)
        {
            GameData.timer = 60;
        }
        else if (dropdown3.value == 3)
        {
            GameData.timer = 90;
        }
        else if (dropdown3.value == 4)
        {
            GameData.timer = 120;
        }
        else if (dropdown3.value == 5)
        {
            GameData.timer = 150;
        }
    }

    public void Jotaro()
    {
        GameData.dioAI = true;
        GameData.jojoAI = false;
        card1.fontSize = 33;
        card2.fontSize = 25;
        card1.color = Color.white;
        card2.color = Color.grey;
    }
    public void Dio()
    {
        GameData.dioAI = false;
        GameData.jojoAI = true;
        card1.fontSize = 27;
        card2.fontSize = 28;
        card1.color = Color.grey;
        card2.color = Color.white;
    }
    public void CloseSingleplayer()
        {
            SingleplayerUI.SetActive(false);
        }
    }
