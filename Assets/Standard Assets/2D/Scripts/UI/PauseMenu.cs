﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    public bool cinematic = false;
    public static bool IsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject controlsMenuUI;
    public GameObject endMenuUI;
    protected GameObject player1;
    protected GameObject player2;
    public Button btn;
    public EventSystem es;
    public GameObject SettingsUI;
    public Button btn2;
    private void Start()
    {
        player1 = GameObject.FindGameObjectWithTag("Player1");
        player2 = GameObject.FindGameObjectWithTag("Player2");
        Resume();

    }
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Escape)||Input.GetKeyDown(KeyCode.KeypadMinus))&&!cinematic)
        {
            if(IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        Cursor.visible = false;
        pauseMenuUI.SetActive(false);
        SettingsUI.SetActive(false);
        controlsMenuUI.SetActive(false);
        Time.timeScale = 1;
        IsPaused = false;
        MonoBehaviour[] scripts = player1.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = true;
        }
        MonoBehaviour[] scripts2 = player2.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts2)
        {
            script.enabled = true;
        }
    }
    public void RestartScreen()
    {
        Cursor.visible = true;
        pauseMenuUI.SetActive(false);
        SettingsUI.SetActive(false);
        controlsMenuUI.SetActive(false);
        endMenuUI.SetActive(true);
        Time.timeScale = 0;
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(btn2.gameObject);
        MonoBehaviour[] scripts = player1.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
        MonoBehaviour[] scripts2 = player2.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts2)
        {
            script.enabled = false;
        }
    }
    public void Restart()
    {
        GameData.rounda = 0;
        GameData.roundb = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    void Pause()
    {
        Cursor.visible = true;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        IsPaused = true;
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(btn.gameObject);
        MonoBehaviour[] scripts = player1.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
        MonoBehaviour[] scripts2 = player2.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts2)
        {
            script.enabled = false;
        }
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void OpenSettings()
    {
        SettingsUI.SetActive(true);
    }
}
