﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class SettingsMenu : MonoBehaviour
{
    public GameObject SettingsUI;
    public GameObject ControlsUI;
    public AudioMixer soundsMixer;
    public AudioMixer musicMixer;
    public Slider soundSlider;
    public Slider musicSlider;
    public Toggle fullscreenToggle;
    public TMPro.TMP_Dropdown dropdown;
    public TMPro.TMP_Dropdown dropdown2;
    Resolution[] resolutions;
    public SpriteRenderer extended;
    public SpriteRenderer laptop;
    private void Start()
    {
        soundSlider.value = PlayerPrefs.GetFloat("SoundVolume");
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
        fullscreenToggle.isOn = PlayerPrefs.GetInt("Fullscreen") == 1 ? true : false;
        //resolution
        resolutions = Screen.resolutions;
        int currentResIndex = 0;
        dropdown.ClearOptions();
        List<string> options = new List<string>();
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        if (PlayerPrefs.GetInt("ResolutionChanged") != 0)
            currentResIndex = PlayerPrefs.GetInt("ResolutionIndex");
        dropdown.AddOptions(options);
        dropdown.value = currentResIndex;
        dropdown.RefreshShownValue();

        //keybinds
        if (PlayerPrefs.GetString("Controls") == "Laptop")
        {
            extended.enabled = false;
            laptop.enabled = true;
            dropdown2.value = 1;
        }
        else
        {
            extended.enabled = true;
            laptop.enabled = false;
            dropdown2.value = 0;
        }
    }
    public void SetSoundVolume(float volume)
    {
        soundsMixer.SetFloat("SoundVolume", volume);
        PlayerPrefs.SetFloat("SoundVolume", volume);
    }
    public void SetMusicVolume(float volume)
    {
        musicMixer.SetFloat("MusicVolume", volume);
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }
    public void CloseSettings()
    {
        SettingsUI.SetActive(false);
    }
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        if (isFullscreen)
            PlayerPrefs.SetInt("Fullscreen", 1);
        else
            PlayerPrefs.SetInt("Fullscreen", 0);
    }
    public void SetResolution(int index)
    {
        Resolution resolution = resolutions[index];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetInt("ResolutionIndex", index);
        PlayerPrefs.SetInt("ResolutionChanged", 1);
    }

    public void Extended(int index)
    {
        if (dropdown2.value == 1)
        {
            PlayerPrefs.SetString("Controls", "Laptop");
            extended.enabled = false;
            laptop.enabled = true;
        }
        else if (dropdown2.value == 0)
        {
            PlayerPrefs.SetString("Controls", "Extended");
            extended.enabled = true;
            laptop.enabled = false;
        }
    }

    public void ControlsButton()
    {
        SettingsUI.SetActive(false);
        ControlsUI.SetActive(true);
    }
    public void CloseControl()
    {
        ControlsUI.SetActive(false);
    }
}