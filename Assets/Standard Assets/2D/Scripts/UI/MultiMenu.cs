﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MultiMenu : MonoBehaviour
{
    public GameObject MultiplayerUI;
    public TMPro.TMP_Dropdown dropdown;
    public TMPro.TMP_Dropdown dropdown2;
    public TMPro.TMP_Dropdown dropdown3;
    void Start()
    {
        int rInt = Random.Range(1, 3);
        GameData.map = rInt;
        int rInt2 = Random.Range(1, 4);
        GameData.music = rInt2;
        GameData.timer = 120;
        dropdown3.value = 4;
    }
    public void Multi()
    {
        Debug.Log(GameData.music);
        GameData.dioAI = false;
        GameData.jojoAI = false;
        if (GameData.map == 1)
            SceneManager.LoadScene("DioCastle");
        else
            SceneManager.LoadScene("Clocktower");
    }
    public void Map()
    {
        if (dropdown.value == 2)
        {
            GameData.map = 2;
        }
        else if (dropdown.value == 1)
        {
            GameData.map = 1;
        }
        else if (dropdown.value == 0)
        {
            int rInt = Random.Range(1, 3);
            GameData.map = rInt;
        }
    }
    public void Music()
    {
        if (dropdown2.value == 3)
        {
            GameData.music = 3;
        }
        else if (dropdown2.value == 2)
        {
            GameData.music = 2;
        }
        else if (dropdown2.value == 1)
        {
            GameData.music = 1;
        }
        else if (dropdown2.value == 0)
        {
            int rInt = Random.Range(1, 4);
            GameData.music = rInt;
        }
    }
    public void Time()
    {
        if (dropdown3.value == 0)
        {
            GameData.timer = -99;
        }
        else if (dropdown3.value == 1)
        {
            GameData.timer = 30;
        }
        else if (dropdown3.value == 2)
        {
            GameData.timer = 60;
        }
        else if (dropdown3.value == 3)
        {
            GameData.timer = 90;
        }
        else if (dropdown3.value == 4)
        {
            GameData.timer = 120;
        }
        else if (dropdown3.value == 5)
        {
            GameData.timer = 150;
        }
    }
        public void CloseMultiplayer()
        {
            MultiplayerUI.SetActive(false);
        }
    
}
