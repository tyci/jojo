﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class JojoHurt : PlayersHurt
{

    //counter
    private PlayerControl player2;

    //stand
    private Animator Standanim;

    protected override void Start()
    {
        base.Start();
        stand = GameObject.FindGameObjectWithTag("Stand1").GetComponent<StandControl>();
        Standanim = stand.GetComponent<Animator>();

        //counter
        player2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerControl>();
    }




    public IEnumerator Teleport()
    {
        attack.enabled = false;
        player.enabled = false;
        anim.SetBool("Special1b",true);
        yield return new WaitForSeconds(.3f);
        anim.SetBool("Special1b", false);

    }

    public override void Damage(int dmg)
    {
        base.Damage(dmg);

        if (dmg == 4)  //time stop stop
        {
            enabled = true;
            anim.enabled = true;
            attack.enabled = true;
            anim.speed = 1;
            rb2d.constraints = RigidbodyConstraints2D.None;
            rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
            Standanim.speed = 1;
            Standanim.enabled = true;

        }

    }


    public IEnumerator StarPunch()
    {
        stand.transform.localPosition = new Vector2(-0.4f,0.1f);
        stand.special2 = true;
        yield return new WaitForSeconds(.4f);
        attack.attackTrigger.enabled = true;
        attack.combo = true;
        yield return new WaitForSeconds(.4f);
        attack.combo = false;
        attack.attackTrigger.enabled = false;
        player.enabled = true;
        attack.enabled = true;
        yield return new WaitForSeconds(.2f);
        stand.transform.localPosition = new Vector2(0, 0.1f);
        stand.special2 = false;

    }

    public void Counter()
    {
        sounds.SendMessageUpwards("sound3");
        attack.special1 = false;
        StartCoroutine("Teleport");
        anim.SetBool("Special1", false);
        

        //   if (player.transform.position.x > -14&& player.transform.position.x < -7.5)
        if (player2.transform.position.x < player.transform.position.x)
            {
                player.transform.position = new Vector3(player2.transform.position.x - .2f, player.transform.position.y, player.transform.position.z);
                transform.localScale = new Vector3(2, 2, 2);
            }
            else
            {
                player.transform.position = new Vector3(player2.transform.position.x + .2f, player.transform.position.y, player.transform.position.z);
                transform.localScale = new Vector3(-2, 2, 2);
            }

        StartCoroutine("StarPunch");
    }
}
