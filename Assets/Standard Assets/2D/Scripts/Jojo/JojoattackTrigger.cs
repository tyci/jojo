﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JojoattackTrigger : AttackTrigger
{
    protected override void Start()
    {
        base.Start();
        plr = gameObject.GetComponentInParent<JojoAttack>();
    }
    protected override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);

        //ora ora
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.special2 && col.attachedRigidbody.gravityScale != 3.1f)
        {

            Debug.Log("knoock");
            StartCoroutine(Wait());
            camShake.Shake(0.1f, 0.1f);

            if (plr.special2b)
            {
                if (col.attachedRigidbody.mass != 1.5f)
                    col.SendMessageUpwards("Damage", 5);
                else
                { 
                    col.SendMessageUpwards("Damage", 1);
                    col.SendMessageUpwards("Defence");
                }

            if (col.transform.position.x < plr.transform.position.x)
                {
                    col.SendMessageUpwards("Knockback", 1);
                }
                else
                {
                    col.SendMessageUpwards("Knockback", 2);
                }
            }
            attack.size = new Vector2(0, 0);
        }

        //charge attack
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && ((JojoAttack)plr).charge1 && col.attachedRigidbody.gravityScale != 3.1f)
        {

            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 3);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 4);
            }

            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            camShake.Shake(0.1f, 0.2f);

        }
        //uppercut
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && ((JojoAttack)plr).charge2 && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 1f)    //not block
            {
                col.SendMessageUpwards("Damage", 5);
            }
                if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 7);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 8);
            }

            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
           // attack.size = new Vector2(0, 0);
            camShake.Shake(0.1f, 0.2f);
        }

    }

    public IEnumerator Wait()
    {
        Debug.Log("cor");
        
        yield return new WaitForSeconds(.3f);
        attack.enabled = false;
        if(plr.special2)
            attack.size = new Vector2(2, 1.6f);
        //attack.enabled = true;
    }

}