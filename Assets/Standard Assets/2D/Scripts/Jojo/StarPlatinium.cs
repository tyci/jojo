﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPlatinium : StandControl
{
    public bool special3;
    public bool special3b;
    protected Rigidbody2D rb2d;
    protected BoxCollider2D col;
    public BoxCollider2D attackTrigger;

    protected bool grounded = true;

    protected override void Start()
    {
        base.Start();
        rb2d = GetComponent<Rigidbody2D>();
        col = GetComponent<BoxCollider2D>();
    }
    protected override void Update()
    {
        base.Update();
        anim.SetBool("Special3", special3);

        if (grounded)
        {
            anim.SetBool("Special3b", special3b);
        }

        if (special3&&!anim.GetBool("Special3b"))
        {
            Charge();
        }
        else if (!special3)
        {
            transform.localPosition = new Vector2(0, 0.1f);
        }

    }

    void Charge()
    {
        if (grounded)
        {
            transform.localPosition = new Vector2(transform.localPosition.x + .1f, transform.localPosition.y);
            attackTrigger.transform.localPosition= new Vector2(transform.localPosition.x + .1f, transform.localPosition.y);
        }
        else
        {
            transform.localPosition = new Vector2(transform.localPosition.x + .07f, transform.localPosition.y - .08f);
            attackTrigger.transform.localPosition = new Vector2(transform.localPosition.x + .07f, transform.localPosition.y - .08f);
        }
    }



    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Ground") || col.CompareTag("Platform"))
        {
           grounded = true;
        }

    }
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Ground") || col.CompareTag("Platform"))
        {
            grounded = true;
        }

    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Ground") || col.CompareTag("Platform"))
        {
            grounded = false;

        }
    }
}