﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JojoAttack : PlayersAttack
{
    public bool charge1 = false;
    public bool charge2 = false;
    protected override void Start()
    {
        base.Start();
        stand = GameObject.FindGameObjectWithTag("Stand1").GetComponent<StarPlatinium>();

        attackCd = 0.4f;
        attackCd2 = 0.9f;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        //combolast

        if (!Attacking && !combo1 && combo2 && anim.GetBool("Ground"))
        {
            combo = true;
            player.enabled = false;

            if (attackTimer3 > 0)
            {

                attackTimer3 -= Time.deltaTime;

                if (attackTimer3 < attackCd2 / 1.5)
                {
                    attackTrigger.enabled = true;
                }
                if (attackTimer3 < attackCd2 / 4)
                {
                    attackTrigger.enabled = false;
                }
            }
            else
            {
                combo2 = false;
                player.enabled = true;
                combo = false;
            }
        }
      
       
        else if (special1)  //yare yare
        {
            player.enabled = false;
            if (specialTimer > 0)
            {
                specialTimer -= Time.deltaTime;
                if (specialTimer < .8f)
                    rb2d.mass = 2f;

                if (specialTimer < .1f)
                    rb2d.mass = 1f;

            }
            else
            {
                player.enabled = true;
                anim.SetBool("Special1", false);
                special1 = false;
                rb2d.mass = 1f;

            }

        }
        else if (special2)  //ora ora ora
        {

            if (specialTimer > 0)
            {
                player.enabled = false;
                specialTimer -= Time.deltaTime;
                stand.special1 = true;

                if (specialTimer < .9f )
                 attackTrigger.enabled = true;

                if (specialTimer < .5f)
                {
                   /* if (Input.GetKeyDown(KeyCode.E) && player.curmana >= 25)
                    {
                        player.curmana -= 25;
                        specialTimer += .6f;
                        sounds.SendMessageUpwards("stop");
                        sounds.SendMessageUpwards("sound5");
                    }*/
                }

                if (specialTimer < .3f)
                {
                    special2b = true;
                }
                    

                }
            else
            {
                player.enabled = true;
                special2 = false;
                special2b = false;
                stand.special1 = false;
                attackTrigger.offset = new Vector2(0, 0);
                attackTrigger.size = new Vector2(1, 1);
                attackTrigger.enabled = false;
            }
        }
        else if (special3)  //charge attack
        {
            player.enabled = false;
            if (specialTimer > 0)
            {
                specialTimer -= Time.deltaTime;

                if (specialTimer < .7f)
                {
                    ((StarPlatinium)stand).special3 = true;
                    attackTrigger.enabled = true;
                    charge1 = true;
                }
                if (specialTimer < .42f)
                {
                    ((StarPlatinium)stand).special3b = true;
                    charge1 = false;
                    charge2 = true;
                }
            }
            else
            {
                StopCoroutine("TriggerSize");
                player.enabled = true;
                attackTrigger.enabled = false;
                attackTrigger.transform.localPosition = new Vector2(.26f, -0.095f);
                attackTrigger.size = new Vector2(1, 1);
                anim.SetBool("Special3", false);
                special3 = false;
                ((StarPlatinium)stand).special3 = false;
                ((StarPlatinium)stand).special3b = false;
                charge1 = false;
                charge2 = false;
            }

        }


        //yare  yare
        if (Input.GetKeyDown(StandKey) && Input.GetKey(AttackDirDown) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && (player.curmana >= 30 || standOn) && !special2 && !special1 && !special3)
        {
            special1 = true;
            attackTrigger.enabled = false;
            specialTimer = 1f;
            Attacking = false;
            combo1 = false;
            combo2 = false;
            player.enabled = false;
            sounds.SendMessageUpwards("sound2");
            if(!standOn)
                player.curmana -= 30;
            anim.SetBool("Special1", special1);
        }


        //ora ora ora
        if (Input.GetKeyDown(StandKey) && Input.GetKey(AttackDirUp) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && (player.curmana>=40 || standOn) && !special1 && !special2 && !special3)
        {
            special2 = true;
            attackTrigger.enabled = false;
            specialTimer = 1.2f;
            Attacking = false;
            combo1 = false;
            combo2 = false;
            
            attackTrigger.offset = new Vector2(1.3f, .3f);
            attackTrigger.size = new Vector2(2, 1.6f);
            if (!standOn)
                player.curmana -= 40;
            sounds.SendMessageUpwards("sound4");
            SoundController.SendMessageUpwards("sound10");
        }

        //charge attack
        if (Input.GetKeyDown(StandKey) && (Input.GetKey(AttackDirForward)|| Input.GetKey(AttackDirBack)) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !PreCharge && !kick && !anim.GetBool("Guard") && anim.GetBool("Ground") && (player.curmana >= 40 || standOn) && !special1 && !special2 && !special3)
        {
            if (player.facingLeft && Input.GetKey(AttackDirForward))
            {
                transform.localScale = new Vector3(2, 2, 2);
                player.facingLeft = false;
            }
            special3 = true;
            attackTrigger.enabled = false;
            Attacking = false;
            combo1 = false;
            combo2 = false;
            specialTimer = .9f;
            if (!standOn)
                player.curmana -= 35;
            StartCoroutine("TriggerSize");
            sounds.SendMessageUpwards("sound7");
            SoundController.SendMessageUpwards("sound10");
            anim.SetBool("Special3", special3);
        }



        anim.SetBool("Attack3", combo2);
        anim.SetBool("Special2", special2);

    }
    public IEnumerator TriggerSize()
    {

        yield return new WaitForSeconds(.45f);
        attackTrigger.size = new Vector2(2, 1);
    }
}
