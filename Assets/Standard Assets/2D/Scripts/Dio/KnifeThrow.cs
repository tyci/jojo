﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeThrow : MonoBehaviour
{
    public GameObject Bullet_Emitter;
    public GameObject Bullet;
    private Rigidbody2D player;
    public float Bullet_Forward_Force;
    public bool flying = false;
    public bool knives = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player2").GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (flying)
        {
            GameObject Temporary_Bullet_Handler;
            Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_Emitter.transform.position, Bullet_Emitter.transform.rotation) as GameObject;

            Rigidbody2D Temporary_RigidBody;
            Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody2D>();
            if (knives)
            {
                Temporary_RigidBody.mass = 1.10f;
                knives = false;
                Temporary_Bullet_Handler.transform.localPosition = new Vector2(Temporary_Bullet_Handler.transform.localPosition.x, Temporary_Bullet_Handler.transform.localPosition.y - 0.2f);
            }
            
            Temporary_RigidBody.constraints = RigidbodyConstraints2D.FreezePositionY;
            if (player.transform.position.x < Temporary_RigidBody.transform.position.x)
                Temporary_RigidBody.velocity = new Vector3(15, 0, transform.position.z);
            else if (player.transform.position.x > Temporary_RigidBody.transform.position.x)
            {
                Temporary_RigidBody.velocity = new Vector3(-15, 0, transform.position.z);
                Temporary_Bullet_Handler.transform.Rotate(0, 180, 0);
            }

            Destroy(Temporary_Bullet_Handler, 3.0f);
            flying = false;

        }
    }

}

