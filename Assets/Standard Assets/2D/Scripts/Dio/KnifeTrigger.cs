﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeTrigger : MonoBehaviour
{
    protected CameraShake camShake;
    protected Sounds sounds;
    protected bool many = false;
    public Sprite mySprite;
    void Start()
    {
        camShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        if (GetComponent<Rigidbody2D>().mass == 1.10f)
        {
            GetComponent<SpriteRenderer>().sprite = mySprite;
            many = true;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && col.attachedRigidbody.gravityScale != 3.1f  && many==false)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 1);
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Defence");      //block
            }


            int rInt = Random.Range(1, 6);
            sounds.SendMessageUpwards("random", rInt);
            Debug.Log("dmg");
            camShake.Shake(0.1f, 0.1f);

            Destroy(gameObject);
        }
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && col.attachedRigidbody.gravityScale != 3.1f && many)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }
            else
            {
                col.SendMessageUpwards("Damage", 1);
                col.SendMessageUpwards("Damage", 5);
            }


            if (col.transform.position.x < transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 3);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 4);
            }


            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            Debug.Log("dmg2");
            camShake.Shake(0.2f, 0.1f);

            many = false;
            Destroy(gameObject);
        }
    }

    }
