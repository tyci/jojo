﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DioAttack : PlayersAttack
{
    public float specialTimer2 = 0;

    public KnifeThrow knifeThrow;
    public bool special3b = false;

    //zawarudo
    private Animator anim2;
    private Animator Standanim2;
    private PlayerControl player2;
    private PlayersAttack attack2;
    private Rigidbody2D rb2d2;

    protected override void Start()
    {
        base.Start();
        stand = GameObject.FindGameObjectWithTag("Stand2").GetComponent<StandControl>();
        knifeThrow = GetComponent<KnifeThrow>();
        //zawarudo
        anim2 = GameObject.FindGameObjectWithTag("Player1").GetComponent<Animator>();
        Standanim2 = GameObject.FindGameObjectWithTag("Stand1").GetComponent<Animator>();
        rb2d2 = GameObject.FindGameObjectWithTag("Player1").GetComponent<Rigidbody2D>();
        attack2= GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayersAttack>();

        attackCd = 0.4f;
        attackCd2 = 1.0f;
        player2 = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerControl>();
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        //combolast

        if (!Attacking && !combo1 && combo2 && anim.GetBool("Ground"))
        {
            combo = true;
            player.enabled = false;

            if (attackTimer3 > 0)
            {

                attackTimer3 -= Time.deltaTime;
                

                if (attackTimer3 < attackCd2 / 1.5)
                {
                    if(player.transform.position.x>attackTrigger.transform.position.x)
                    rb2d.AddForce(new Vector3(20, 0, transform.position.z));
                    else
                    rb2d.AddForce(new Vector3(-20, 0, transform.position.z));
                    attackTrigger.enabled = true;
                }

                if (attackTimer3 < attackCd2 / 4)
                {
                    rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                    attackTrigger.enabled = false;
                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                combo2 = false;
                player.enabled = true;
                combo = false;
               

            }
        }

        //special 1 - zawarudo
        else if(special1)
        {

            if (specialTimer2 > 0)
            {
                specialTimer2 -= Time.deltaTime;
                if (specialTimer2 > 8.8f)
                {
                    player.enabled = false;
                    anim.SetBool("Special1", special1);
                }
                else if (specialTimer2 < 8.8f && specialTimer2 > 8.1f)
                {
                    stand.effect = true;
                    player.enabled = false;
                    player2.enabled = false;
                    anim2.speed = 0;
                    anim2.enabled = false;
                    Standanim2.speed = 0;
                    Standanim2.enabled = false;
                    attack2.enabled = false;
                    attack2.attackTrigger.enabled = false;

                    MonoBehaviour[] scripts = player2.GetComponents<MonoBehaviour>();
                    foreach (MonoBehaviour script in scripts)
                    {
                        script.enabled = false;
                    }

                    rb2d2.constraints = RigidbodyConstraints2D.FreezeAll;
                }
                else if (specialTimer2 < 8f)
                {
                    if(anim.GetBool("Special1"))
                        player.enabled = true;
                    anim.SetBool("Special1", false);
                    stand.effect = false;
                }

            }
            else
            {
                stand.effect = false;
                player.enabled = true;
                anim.SetBool("Special1", false);
                sounds.SendMessageUpwards("sound2");
                Debug.Log("sstop");
                special1 = false;
                player2.enabled = true;
                anim2.enabled = true;
                anim2.speed = 1;
                attack2.enabled = true;
                Standanim2.speed = 1;
                Standanim2.enabled = true;

                MonoBehaviour[] scripts = player2.GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour script in scripts)
                {
                    script.enabled = true;
                }

                rb2d2.constraints = RigidbodyConstraints2D.None;
                rb2d2.constraints = RigidbodyConstraints2D.FreezeRotation;
            }

        }
        else if (special2)  //muda muda muda
        {

            if (specialTimer > 0)
            {
                specialTimer -= Time.deltaTime;
                stand.special1 = true;
                player.enabled = false;

                if (specialTimer < .9f)
                {
                    attackTrigger.enabled = true;
                    
                }

                if (specialTimer < .5f)
                {
                   /* if (Input.GetKeyDown(KeyCode.Keypad5) && player.curmana >= 25)
                    {
                        player.curmana -= 25;
                        specialTimer += .6f;
                        sounds.SendMessageUpwards("stop");
                        sounds.SendMessageUpwards("sound4");
                    }*/
                }

                if (specialTimer < .3f)
                {
                    special2b = true;
                }


            }
            else
            {
                player.enabled = true;
                special2 = false;
                special2b = false;
                stand.special1 = false;
                attackTrigger.offset-= new Vector2(1.3f, .3f);
                attackTrigger.size = new Vector2(1, 1);
                attackTrigger.enabled = false;
            }
        }
        else if (special3b)  //knife
        {
            player.enabled = false;
            if (specialTimer > 0)
            {
                specialTimer -= Time.deltaTime;
                if (Input.GetKeyDown(StandKey) && (player.curmana >= 10 || standOn) && specialTimer>.2f && !anim.GetBool("Special3b"))
                {
                    if (!standOn)
                        player.curmana -= 10;
                    anim.SetBool("Special3b", true);
                    knifeThrow.knives = true;
                    specialTimer = .75f;
                    sounds.SendMessageUpwards("sound5");
                }
                if (specialTimer < .2f)
                {
                    anim.SetBool("Special3", false);
                    anim.SetBool("Special3b", false);
                    StartCoroutine("KnifeThrowFinish");
                    knifeThrow.flying = true;
                    special3b = false;
                    sounds.SendMessageUpwards("sound8");
                }

            }
           else
           {
               anim.SetBool("Special3", false);
               anim.SetBool("Special3b", false);
                special3b = false;
                special3 = false;
            }

       }

       //special 1 - zawarudo
       if (Input.GetKeyDown(StandKey) && Input.GetKey(AttackDirDown) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && ( player.curmana >= 55 || standOn) && !special1 && !special2 && !special3)
       {
           special1 = true;
           attackTrigger.enabled = false;
           specialTimer2 = 10f;
           Attacking = false;
           combo1 = false;
           combo2 = false;
           player.enabled = false;
           //   audio.Play();
           sounds.SendMessageUpwards("sound1");
           if (!standOn)
               player.curmana -= 55;
       }


       //muda muda muda
       if (Input.GetKeyDown(StandKey) && Input.GetKey(AttackDirUp) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && ( player.curmana >= 40 || standOn) && !special1 && !special2 && !special3)
       {
           special2 = true;
           attackTrigger.enabled = false;
           specialTimer = 1.2f;
           Attacking = false;
           combo1 = false;
           combo2 = false;
           attackTrigger.offset += new Vector2(1.3f, .3f);
           attackTrigger.size += new Vector2(1, 0.6f);
           if (!standOn)
               player.curmana -= 40;
           sounds.SendMessageUpwards("sound3");
           SoundController.SendMessageUpwards("sound10");
       }

       //knife
       if (Input.GetKeyDown(StandKey) && (Input.GetKey(AttackDirForward) || Input.GetKey(AttackDirBack)) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && (player.curmana >= 15 || standOn) && !special1 && !special2 && !special3)
       {
            if (player.facingLeft && Input.GetKey(AttackDirForward))
            {
                transform.localScale = new Vector3(2, 2, 2);
                player.facingLeft = false;
            }
            special3 = true;
            special3b = true;
            attackTrigger.enabled = false;
           Attacking = false;
           combo1 = false;
           combo2 = false;
           specialTimer = .6f;
           if (!standOn)
               player.curmana -= 15;
           sounds.SendMessageUpwards("sound7");
           anim.SetBool("Special3", special3);
            
        }

        //zawarudo smashesss
        //uppercut
        if (Input.GetKeyDown(AttackButton) && Input.GetKey(AttackDirUp) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !PreCharge && !kick && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special2 && !special3 && (player.curmana >= 10 || standOn))
        {
            attackTimer3 = .9f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            uppercut = true;
            attackTrigger.size += new Vector2(.8f, 0);
            anim.SetBool("Combo", uppercut);
            if (!standOn)
                player.curmana -= 10;
            SoundController.SendMessageUpwards("sound10");
        }

        //front attack
        if (Input.GetKeyDown(AttackButton) && (Input.GetKey(AttackDirForward) || Input.GetKey(AttackDirBack)) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special2 && !special3 && (player.curmana >= 10 || standOn))
        {
            if (player.facingLeft && Input.GetKey(AttackDirForward))
            {
                transform.localScale = new Vector3(2, 2, 2);
                player.facingLeft = false;
            }
            attackTimer3 = .9f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            frontattack = true;
            attackTrigger.size += new Vector2(.8f, 0);
            anim.SetBool("Combo2", frontattack);
            if (!standOn)
                player.curmana -= 10;
            SoundController.SendMessageUpwards("sound10");
        }

        //kick
        if (Input.GetKeyDown(AttackButton) && Input.GetKey(AttackDirDown) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special2 && !special3 && (player.curmana >= 10 || standOn))
        {
            attackTimer3 = .6f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            kick = true;
            anim.SetBool("Combo3", kick);
            if (!standOn)
                player.curmana -= 10;
        }


        anim.SetBool("Attack3", combo2);
        anim.SetBool("Special2", special2);




    }
    public IEnumerator KnifeThrowFinish()
    {
        yield return new WaitForSeconds(.3f);
        player.enabled = true;
        special3 = false;
        specialTimer = 0;
    }
}
