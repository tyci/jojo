﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DioattackTrigger : AttackTrigger
{
    protected DioAttack player;
    protected override void Start()
    {
        base.Start();

        player = gameObject.GetComponentInParent<DioAttack>();
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);

        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && !plr.special2 && col.attachedRigidbody.gravityScale != 3.1f)     //basic attack
        {
            if (plr.specialTimer > 0.1f)    //stop time
                col.SendMessageUpwards("Damage", 4);
            player.specialTimer2 = 0f;
        }


        //muda muda
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.special2 && col.attachedRigidbody.gravityScale != 3.1f)
        {

            Debug.Log("knoock");
            StartCoroutine(Wait());
            camShake.Shake(0.1f, 0.1f);

            if (plr.special2b)
            {

                if (col.attachedRigidbody.mass != 1.5f)
                    col.SendMessageUpwards("Damage", 5);
                else
                {
                    col.SendMessageUpwards("Damage", 1);
                    col.SendMessageUpwards("Defence");
                }

                if (col.transform.position.x < plr.transform.position.x)
                {
                    col.SendMessageUpwards("Knockback", 1);
                }
                else
                {
                    col.SendMessageUpwards("Knockback", 2);
                }
            }
            attack.size = new Vector2(0, 0);
        }
    }

    public IEnumerator Wait()
    {
        Debug.Log("cor");

        yield return new WaitForSeconds(.3f);
        attack.enabled = false;
        //attack.enabled = true;
        if(plr.special2)
        attack.size = new Vector2(2, 1.6f);
    }

}