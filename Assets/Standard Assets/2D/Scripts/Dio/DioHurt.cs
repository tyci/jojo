﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DioHurt : PlayersHurt
{
    protected DioAttack dioattack;

    protected override void Start()
    {
        base.Start();
        stand = GameObject.FindGameObjectWithTag("Stand2").GetComponent<StandControl>();
        dioattack = GetComponent<DioAttack>();
    }

    public override void Damage(int dmg)
    {
        base.Damage(dmg);
        dioattack.specialTimer2 = 0f;

    }
    }