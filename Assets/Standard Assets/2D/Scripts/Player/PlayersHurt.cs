﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayersHurt : MonoBehaviour
{
    protected Rigidbody2D rb2d;
    protected Animator anim;
    protected PlayerControl player;
    protected PlayersAttack attack;
    protected bool falling = false;

    protected Sounds sounds;
    protected Sounds SoundController;

    protected SpriteRenderer spriterenderer;
    protected EffectController effects;
    //stand
    protected StandControl stand;
    protected virtual void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        player = GetComponent<PlayerControl>();
        attack = GetComponent<PlayersAttack>();
        sounds = gameObject.GetComponentInChildren<Sounds>();
        SoundController = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        effects = gameObject.GetComponentInChildren<EffectController>();
        spriterenderer = GetComponent<SpriteRenderer>();
    }


    protected virtual void Update()
    {
        if (falling && !player.grounded)
        {
            stand.defend = false;
            player.enabled = false;
            attack.enabled = false;
            attack.combo = false;
            attack.attackTrigger.enabled = false;
            attack.combo1 = false;
            attack.combo2 = false;
            attack.Attacking = false;
            attack.attackTimer3 = 0;
            attack.specialTimer = 0f;
            attack.Charging = false;
            attack.PreCharge = false;
            stand.special1 = false;
            stand.special2 = false;
            anim.SetInteger("Damage", 10);
            //   body.size = new Vector3(.5f, .3f);
            //    ground.offset = new Vector2(0, 2f);
            //    ground.size = new Vector2(2.6f, 1);
        }
        else if (falling && player.grounded)
        {
            anim.SetInteger("Damage", 10);
            StopCoroutine("Hit");
            StartCoroutine("Fall");
        }
    }



    public IEnumerator Fall()
    {
        falling = false;
        //   body.size = new Vector3(.2f, .58f);
        //    ground.offset = new Vector2(0, 1);
        //    ground.size = new Vector2(1, 1);
        yield return new WaitForSeconds(.05f);
         anim.SetInteger("Damage", 9);
        rb2d.mass = 3;
        rb2d.gravityScale = 3.1f;
        yield return new WaitForSeconds(1f);
        anim.SetBool("Crouch", true);
        StartCoroutine("ImmortalFlashing");
        yield return new WaitForSeconds(.45f);
        rb2d.mass = 1;
        player.enabled = true;
        attack.enabled = true;
        falling = false;
        yield return new WaitForSeconds(.01f);
        anim.SetInteger("Damage", 0);
    }

    public IEnumerator Hit()
    {
     
        int dmg = Random.Range(1, 5);
        anim.SetInteger("Damage", dmg);
        player.enabled = false;

        Vector3 easeVelocity = rb2d.velocity;
        easeVelocity.y = rb2d.velocity.y;
        easeVelocity.x *= 0.4f;
        easeVelocity.z = 0.0f;
        rb2d.velocity = easeVelocity;

        stand.defend = false;
        attack.enabled = false;
        attack.attackTrigger.enabled = false;
        attack.combo = false;
        attack.combo1 = false;
        attack.combo2 = false;
        attack.Attacking = false;
        attack.attackTimer3 = 0;
        attack.specialTimer = 0f;
        attack.Charging = false;
        attack.PreCharge = false;
        stand.special1 = false;
        stand.special2 = false;
        yield return new WaitForSeconds(.5f);
        anim.SetInteger("Damage", 0);
        player.enabled = true;
        attack.enabled = true;

    }
    public virtual void Damage(int dmg)
    {
        sounds.SendMessageUpwards("stop");
        StopCoroutine("Hit");
        int small = Random.Range(1, 4);
        int big = Random.Range(4, 6);
        if (dmg == 3)
        { player.curHealth -= 1; }
        else if (dmg == 4 || dmg == 5)
        { player.curHealth -= 0; }
        else
        {
            player.curHealth -= dmg;
        }


        if (dmg == 2 || (dmg == 3 && !player.grounded) || dmg == 5)
        {
            StartCoroutine("StartFall");
            StartCoroutine("Blood", big);
        }
        else if (dmg == 3 && player.grounded)
        {
            StartCoroutine("Blood", small);
            StartCoroutine("Hit");
        }
        else if (dmg == 1 || dmg == 5)
        {
            StartCoroutine("Blood", small);
            StartCoroutine("Hit");
        }
            

        anim.SetBool("Crouch", false);

    }

    public virtual void Defence()
    {
        if(!attack.standOn)
        player.curmana -= 9;
        StartCoroutine("DefendEffect");
    }

        public IEnumerator Blood(int i)
    {
        effects.blood = i;
        yield return new WaitForSeconds(.01f);
        effects.blood = 0;
    }
    public IEnumerator DefendEffect()
    {
        effects.defend = true;
        yield return new WaitForSeconds(.01f);
        effects.defend = false;
    }
    public IEnumerator StartFall()
    {
        sounds.SendMessage("sound15");
        anim.SetInteger("Damage", 10);
        yield return new WaitForSeconds(.1f);
        falling = true;
    }
    public IEnumerator ImmortalFlashing()
    {
        for (int i = 0; i < 3; i++)
        {
            spriterenderer.color = new Vector4(255, 255, 255, 1);
            yield return new WaitForSeconds(.22f);
            spriterenderer.color = new Vector4(255, 255, 255, .5f);
            yield return new WaitForSeconds(.08f);

        }
        spriterenderer.color = new Vector4(255, 255, 255, 1);
        rb2d.gravityScale = 3;
    }
}