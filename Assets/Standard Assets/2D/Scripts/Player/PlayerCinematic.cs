﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCinematic : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D rb2d;
    private StandPower power;
    [SerializeField] public float speed = 50f;
    [SerializeField] public float jumpPower = 550f;
    public float maxSpeed = 3;
    public bool walking = false;
    public bool facingLeft = false;
    void Start()
    {
        anim = GetComponent<Animator>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        power = GetComponentInChildren<StandPower>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Ground", true);
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
    }
    void FixedUpdate()
    {
        //limit left right speed
        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);

        }
        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }

        //friction
        Vector3 easeVelocity = rb2d.velocity;
        easeVelocity.y = rb2d.velocity.y;
        easeVelocity.x *= 0.8f;
        easeVelocity.z = 0.0f;

        if (walking && !facingLeft)
            rb2d.AddForce(Vector2.right * speed);
        else if (walking && facingLeft)
            rb2d.AddForce(Vector2.left * speed);
    }

    public void Special1()
    {
        anim.SetBool("Special1", true);
        StartCoroutine("AnimStop");
    }
    public IEnumerator AnimStop()
    {
        yield return new WaitForSeconds(.8f);
        anim.speed = 0;
    }

    public void Intro()
    {
        walking = true;
        StartCoroutine("StopWalking");
    }
    public IEnumerator StopWalking()
    {
        yield return new WaitForSeconds(.6f);
        walking = false;
    }
    //trailer:
    public IEnumerator Point()
    {
        anim.SetBool("point", true);
        yield return new WaitForSeconds(.3f);
        power.transform.localPosition = new Vector3(0.061f, -0.014f, 0);
        yield return new WaitForSeconds(3f);
        anim.SetBool("point", false);
    }
    public IEnumerator Stance()
    {
        anim.SetBool("trailer", true);
        yield return new WaitForSeconds(.3f);
        power.transform.localPosition = new Vector3(0.013f, -0.052f, 0);
        yield return new WaitForSeconds(19f);
        anim.SetBool("trailer", false);
    }

    public IEnumerator Charging()
    {
        anim.SetBool("Charging", true);
        yield return new WaitForSeconds(2f);
        anim.SetBool("Charging", false);
    }

    public IEnumerator Talk(float x)
    {
        power.talking = true;
        yield return new WaitForSeconds(x);
        power.talking = false;
    }
}
