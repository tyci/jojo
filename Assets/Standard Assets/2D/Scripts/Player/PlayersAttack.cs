﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum PlayerState
{
    Idle,
    Attack,
    Special
}

public class PlayersAttack : MonoBehaviour
{
    PlayerState State;
    public bool uppercut = false;
    public bool frontattack = false;
    public bool kick = false;
    public bool Attacking = false;
    public bool combo1 = false;
    public bool combo2 = false;
    public bool jumpAttack = false;
    public bool special1 = false;
    public bool special2 = false;
    public bool special2b = false;
    public bool special3 = false;
    public bool combo = false;
    public bool standOn = false;
    public bool Awakening = true;
    public bool Charging = false;
    public bool PreCharge = false;

    public float specialTimer = 0;
    public float AwakeningTimer = 0;


    public KeyCode AttackButton;
    public KeyCode AttackDirUp;
    protected KeyCode AttackDirForward;
    protected KeyCode AttackDirBack;
    public KeyCode AttackDirDown;
    public KeyCode GuardKey;
    public KeyCode StandKey;


    public float attackTimer = 0;
    public float attackTimer2 = 0;
    public float attackTimer3 = 0;
    protected float attackCd;
    protected float attackCd2;

    protected Transform CameraParent;
    protected Camera maincamera;
    protected CameraFollow camerafollow;
    protected Sounds sounds;
    protected Sounds SoundController;

    protected PlayerControl player;
    protected Rigidbody2D rb2d;
    public BoxCollider2D attackTrigger;
    protected Animator anim;

    //stand
    protected StandControl stand;
    public StandPower standpower;

    protected virtual void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        attackTrigger.enabled = false;
        sounds = gameObject.GetComponentInChildren<Sounds>();
        SoundController = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
        standpower = gameObject.GetComponentInChildren<StandPower>();
        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        CameraParent = maincamera.GetComponentInParent<Transform>();
        camerafollow = maincamera.GetComponentInParent<CameraFollow>();

        player = GetComponent<PlayerControl>();
        rb2d = player.GetComponent<Rigidbody2D>();
        AttackDirBack = player.LeftKey;
        AttackDirForward = player.RightKey;
    }
    protected virtual void FixedUpdate()
    {
        //attack1
        if (!Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !jumpAttack && !PreCharge && !special3
            && !Input.GetKey(AttackDirUp) && Input.GetKeyDown(AttackButton) && (!(Input.GetKey(AttackDirBack) || Input.GetKey(AttackDirForward)) || player.curmana < 10) && !Input.GetKey(AttackDirDown)
            && !anim.GetBool("Guard") && !anim.GetBool("Special1") && !anim.GetBool("Special2") && anim.GetBool("Ground"))
        {
            Attacking = true;
            attackTimer = attackCd;
            player.enabled = false;
            int rInt = Random.Range(13, 16);
            SoundController.SendMessageUpwards("random", rInt);
        }

        if (Attacking && player.grounded == true)
        {
            if (attackTimer > 0)
            {

                attackTimer -= Time.deltaTime;
                attackTrigger.enabled = true;
                if (attackTimer < attackCd / 2)
                {
                    if (Input.GetKeyDown(AttackButton) && combo1 == false)
                    {
                        combo1 = true;
                        attackTimer2 = attackCd;
                        int rInt = Random.Range(13, 16);
                        StartCoroutine("SoundRandom", rInt);
                    }

                }
            }
            else
            {
                Attacking = false;
                player.enabled = true;
                attackTrigger.enabled = false;
                attackTrigger.size = new Vector2(1, 1);
            }

        }

        //attack2

        else if (!Attacking && combo1 && player.grounded == true)
        {
            player.enabled = false;
            if (attackTimer2 > 0)
            {
                attackTimer2 -= Time.deltaTime;
                attackTrigger.enabled = true;
                if (attackTimer2 < attackCd / 2)
                {

                    if (Input.GetKeyDown(AttackButton) && combo2 == false)
                    {
                        combo2 = true;
                        attackTimer3 = attackCd2;
                        sounds.SendMessageUpwards("sound10");
                        int rInt = Random.Range(13, 16);
                        StartCoroutine("SoundRandom", rInt);
                    }
                }
            }
            else
            {
                attackTrigger.size = new Vector2(1, 1);
                combo1 = false;
                player.enabled = true;
                attackTrigger.enabled = false;
            }
        }
        //upcut
        else if (uppercut)
        {
            player.enabled = false;
            if (attackTimer3 > 0)
            {
                attackTimer3 -= Time.deltaTime;

                if (attackTimer3 < 0.7f && attackTimer3 > 0.3f)
                {
                    stand.combo1 = true;
                    attackTrigger.enabled = true;

                }
                if (attackTimer3 < 0.3f)
                {
                    // player.enabled = true;
                    // anim.SetBool("Combo", false);
                    attackTrigger.enabled = false;
                    attackTrigger.size = new Vector2(1, 1);
                }
            }
            else
            {
                attackTrigger.enabled = false;
                stand.combo1 = false;
                uppercut = false;
                player.enabled = true;
                attackTrigger.size = new Vector2(1, 1);
                anim.SetBool("Combo", false);
            }

        }
        //front attack
        else if (frontattack)
        {
            player.enabled = false;
            if (attackTimer3 > 0)
            {
                attackTimer3 -= Time.deltaTime;

                if (attackTimer3 < 0.7f && attackTimer3 > 0.3f)
                {
                    stand.combo2 = true;
                    attackTrigger.enabled = true;

                }
                if (attackTimer3 < 0.3f)
                {
                    // player.enabled = true;
                    // anim.SetBool("Combo", false);
                    attackTrigger.enabled = false;
                    attackTrigger.size = new Vector2(1, 1);
                }
            }
            else
            {
                attackTrigger.enabled = false;
                stand.combo2 = false;
                frontattack = false;
                player.enabled = true;
                attackTrigger.size = new Vector2(1, 1);
                anim.SetBool("Combo2", false);
            }

        }

        else if (jumpAttack)
        {
            if (attackTimer3 > 0)
            {

                attackTimer3 -= Time.deltaTime;
                attackTrigger.enabled = true;
            }
            else
            {
                attackTrigger.enabled = false;

            }

        }
        //kick
        else if (kick)
        {
            player.enabled = false;
            if (attackTimer3 > 0)
            {
                attackTimer3 -= Time.deltaTime;

                if (attackTimer3 < 0.5f && attackTimer3 > 0.2f)
                {
                    attackTrigger.enabled = true;
                }
                if (attackTimer3 < 0.2f)
                {
                    attackTrigger.enabled = false;
                }
            }
            else
            {
                kick = false;
                player.enabled = true;
                attackTrigger.enabled = false;
                anim.SetBool("Combo3", false);
            }
        }

        //defence
        else if (Input.GetKey(GuardKey) && anim.GetBool("Ground") && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !PreCharge && !special1 && !special2 && !special3 )
        {
            if ((player.curmana >= 9 || standOn))
            {
                anim.SetBool("Guard", true);
                player.enabled = false;
                combo = false;
                attackTrigger.enabled = false;
                combo1 = false;
                combo2 = false;
                Attacking = false;
                rb2d.mass = 1.5f;
                stand.defend = true;
            }
            else
            {
                anim.SetBool("Guard", true);
                player.enabled = false;
                combo = false;
                attackTrigger.enabled = false;
                combo1 = false;
                combo2 = false;
                Attacking = false;
                stand.defend = false;
                rb2d.mass = 1f;
            }
        }
        //charge chakra
        else if(Charging)
        {
            player.enabled = false;

            if (Input.GetKey(StandKey))
            {
                
                if (player.curmana<100)
                    player.curmana += (Time.deltaTime * 28f);
                if (standOn == true && player.curmana > 99)
                {
                    StartCoroutine("Awaken");
                    StartCoroutine("CloseUp");
                    Charging = false;
                }
                
            }
            else
            {
                StartCoroutine("ChargeOff");
            }
        }
        else if(!Input.GetKey(GuardKey) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick && !special1 && !special2 && !special3 && !PreCharge)
        {
            anim.SetBool("Guard", false);
            player.enabled = true;
            rb2d.mass = 1.0f;
            stand.defend = false;
        }




        if (!jumpAttack && Input.GetKeyDown(AttackButton) && (player.grounded == false || anim.GetBool("JumpCheck")))
        {
            Debug.Log(!jumpAttack && Input.GetKeyDown(AttackButton) && (player.grounded == false || anim.GetBool("JumpCheck")));
            attackTimer3 = attackCd;
            Attacking = false;
            combo1 = false;
            combo2 = false;
            player.enabled = false;
            jumpAttack = true;
            attackTrigger.offset = new Vector2(0, .5f);
            int rInt = Random.Range(13, 16);
            SoundController.SendMessageUpwards("random", rInt);
        }
        if (rb2d.velocity.y == 0 && player.grounded && jumpAttack)
        {
            player.enabled = true;
            jumpAttack = false;
            attackTrigger.offset = new Vector2(0, 0);
            attackTrigger.enabled = false;

        }
        if (rb2d.velocity.y > 0.3f)
        {
            anim.SetBool("JumpCheck", false);
            //StopCoroutine("PreJump");
        }


        //uppercut
        if (Input.GetKeyDown(AttackButton) && Input.GetKey(AttackDirUp) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !PreCharge && !kick && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special1 && !special2 && !special3 && (player.curmana >= 10 || standOn))
        {
            attackTimer3 = .9f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            uppercut = true;
            attackTrigger.size += new Vector2(.8f, 0);
            anim.SetBool("Combo", uppercut);
            if(!standOn)
                player.curmana -= 10;
            SoundController.SendMessageUpwards("sound10");
        }

        //front attack
        if (Input.GetKeyDown(AttackButton) && (Input.GetKey(AttackDirForward) || Input.GetKey(AttackDirBack)) && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick  && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special1 && !special2 && !special3 && (player.curmana >= 10 || standOn))
        {
            if(player.facingLeft&& Input.GetKey(AttackDirForward))
                {
                transform.localScale = new Vector3(2, 2, 2);
                player.facingLeft = false;
            }
            attackTimer3 = .9f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            frontattack = true;
            attackTrigger.size += new Vector2(.8f, 0);
            anim.SetBool("Combo2", frontattack);
            if (!standOn)
                player.curmana -= 10;
            SoundController.SendMessageUpwards("sound10");
        }

        //kick
        if (Input.GetKeyDown(AttackButton) && Input.GetKey(AttackDirDown)  && !Attacking && !combo1 && !combo2 && !uppercut && !frontattack && !kick  && !PreCharge && !anim.GetBool("Guard") && anim.GetBool("Ground") && !special1 && !special2 && !special3 && (player.curmana >= 10||standOn))
        {
            attackTimer3 = .6f;
            player.enabled = false;
            //sounds.SendMessageUpwards("sound2");
            kick = true;
            anim.SetBool("Combo3", kick);
            if (!standOn)
                player.curmana -= 10;
            int rInt = Random.Range(13, 16);
            SoundController.SendMessageUpwards("random", rInt);
        }


        //StandPower
        if (Charging && Awakening && player.curHealth<=10 && player.curmana>99)
        {
            Awakening = false;
            standOn = true;
            AwakeningTimer = 12f;
        }
        if (standOn)
        {
            standpower.IsOn = true;
        }
        else
        { 
            standpower.IsOn = false;
        }
        if (AwakeningTimer > 0)
            AwakeningTimer -= Time.deltaTime;
        if (AwakeningTimer <= 0 && !Awakening)
            standOn = false;

        //charge chakra
        if (Input.GetKey(StandKey) && !Input.GetKey(AttackDirUp) && !(Input.GetKey(AttackDirBack) || Input.GetKey(AttackDirForward)) && !Input.GetKey(AttackDirDown) && !standOn && !Attacking && !combo1 && !combo2 && !uppercut 
            && !frontattack && !kick && !anim.GetBool("Guard") && !PreCharge &&!Charging&& anim.GetBool("Ground") && !special2 && !special1 && !special3)
        {
            sounds.SendMessageUpwards("sound20");
            Charging = true;
            player.enabled = false;
            combo = false;
            attackTrigger.enabled = false;
            combo1 = false;
            combo2 = false;
            Attacking = false;
            PreCharge = true;
        }
        if (Charging)
            standpower.charging = true;
        else
            standpower.charging = false;



        anim.SetBool("Attack1", Attacking);
        anim.SetBool("Attack2", combo1);
        anim.SetBool("JumpAttack", jumpAttack);
        anim.SetBool("Charging", PreCharge);
    }

    public IEnumerator ChargeOff()
    {
        standpower.charging = false;
        Charging = false;
        sounds.SendMessageUpwards("stop");
        yield return new WaitForSeconds(0.01f);
        player.enabled = true;
        PreCharge = false;
    }
    public IEnumerator CloseUp()
    {
        SoundController.SendMessageUpwards("sound12");
        camerafollow.enabled = false;
        Debug.Log(camerafollow.enabled);
        Time.timeScale = 0.3f;
        float temp = maincamera.orthographicSize;
        maincamera.orthographicSize = 1;
         Vector3 pos = CameraParent.transform.position;
        CameraParent.transform.position = new Vector3(transform.position.x, transform.position.y,-10);

        var gameObject = new GameObject();
        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
        var sprite = Resources.Load<Sprite>("anime");
        spriteRenderer.sprite = sprite;
        gameObject.transform.position = transform.position;
        gameObject.transform.localScale = new Vector2(.75f, .75f);
        gameObject.layer = 30;

        yield return new WaitForSeconds(.5f);
        camerafollow.enabled = true;
        CameraParent.transform.position = pos;
        maincamera.orthographicSize = temp;
        Time.timeScale = 1;
        Destroy(gameObject);
        Debug.Log(camerafollow.enabled);

    }
    public IEnumerator Awaken()
    {
        standpower.charging = false;
        Charging = false;
        anim.SetBool("Awaken", true);
        stand.awaken = true;
        SoundController.SendMessageUpwards("sound10");
        sounds.SendMessageUpwards("stop");


        yield return new WaitForSeconds(.1f);
        sounds.SendMessageUpwards("sound6");
        yield return new WaitForSeconds(.5f);
        stand.awaken = false;
        PreCharge = false;
        player.enabled = true;
        anim.SetBool("Awaken", false);
    }
    public IEnumerator SoundRandom(int i)
    {
        yield return new WaitForSeconds(.2f);
        SoundController.SendMessageUpwards("random", i);
    }
}
