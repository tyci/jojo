﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandControl : MonoBehaviour
{
    protected Animator anim;
    protected SpriteRenderer sprite;
    public bool special1;
    public bool special2;
    public bool combo1;
    public bool combo2;
    public bool effect;
    public bool awaken;
    public bool defend;
    protected virtual void Start()
    {
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    protected virtual void Update()
    {
        anim.SetBool("Special1", special1);
            anim.SetBool("Special2", special2);
            anim.SetBool("Combo1", combo1);
            anim.SetBool("Combo2", combo2);
            anim.SetBool("Effect", effect);
            anim.SetBool("Awaken", awaken);
            anim.SetBool("Defend", defend);

        if (defend)
            sprite.sortingOrder = 4;
        else
            sprite.sortingOrder = 4;
    }
}
