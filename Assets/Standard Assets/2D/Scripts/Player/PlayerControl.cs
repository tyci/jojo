﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerControl : MonoBehaviour
{
    protected PlayersAttack attack;
    public Animator anim;
    public Rigidbody2D rb2d;
    public string HorizontalAxisName;
    public KeyCode JumpKey;
    public KeyCode LeftKey;
    public KeyCode RightKey;
    public KeyCode DashKey;
    public bool doubletapL=false;
    public bool doubletapR = false;
    protected Sounds sounds;
    [SerializeField] public float speed = 50f;
    [SerializeField] public float jumpPower = 550f;
    public float maxSpeed = 3;
    public bool grounded;
    //stats
    public float maxmana = 100;
    public float curmana = 0;
    public int curHealth;
    public int maxHealth = 20;
    public bool doubleJump = false;
    public bool facingLeft = false;
    private void Start()
    {
        attack = GetComponent<PlayersAttack>();
        anim = GetComponent<Animator>();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        curmana = maxmana;
        curHealth = maxHealth;
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
    }

    void Update()
    {
        anim.SetBool("Ground", grounded);
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));

        //turning around
        if (Input.GetAxis(HorizontalAxisName) < -0.1f)
        {
            transform.localScale = new Vector3(-2, 2, 2);
            facingLeft = true;
        }
        if (Input.GetAxis(HorizontalAxisName) > 0.1f)
        {
            transform.localScale = new Vector3(2, 2, 2);
            facingLeft = false;
        }

        //jump
        if (Input.GetKeyDown(JumpKey) && grounded && !anim.GetBool("Dash"))
        {
            StartCoroutine("PreJump");
            StartCoroutine("AttackStop");
        }
        if (rb2d.velocity.y != 0)
        {
            anim.SetBool("JumpCheck", false);
            StopCoroutine("PreJump");
        }

        //double jump
        if (Input.GetKeyDown(JumpKey) && !grounded && !anim.GetBool("Dash")&&!doubleJump)
        {
            doubleJump = true;
            StartCoroutine("DoubleJump");
        }
        if (grounded)
            doubleJump = false;

        //stats
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }
        if (curmana > maxmana)
        {
            curmana = maxmana;
        }
        curmana += (Time.deltaTime * 4.5f);

    }


    void FixedUpdate()
    {
        //move
        float h = Input.GetAxis(HorizontalAxisName);
        rb2d.AddForce(Vector2.right * speed * h);

        //limit left right speed
        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);

        }
        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }

        //friction
        Vector3 easeVelocity = rb2d.velocity;
        easeVelocity.y = rb2d.velocity.y;
        easeVelocity.x *= 0.8f;
        easeVelocity.z = 0.0f;

        if (!Input.GetKey(LeftKey) && !Input.GetKey(RightKey) && grounded)
            rb2d.velocity = easeVelocity;




        //dash
        Vector2 fwd = new Vector2(0, -1);
        Vector2 pos = new Vector2(transform.position.x, transform.position.y);
        if (transform.localScale.x > 0)
        {
            pos=new Vector2(transform.position.x + .9f, transform.position.y);
        }
       else if (transform.localScale.x < 0)
        {
            pos = new Vector2(transform.position.x - .9f, transform.position.y);
        }
        Debug.DrawRay(pos, fwd ,Color.red);
        RaycastHit2D hit= Physics2D.Raycast(pos, fwd,1);


        if (Input.GetKeyDown(DashKey)&& Input.GetKey(RightKey) && !anim.GetBool("Dash"))
             StartCoroutine("DashRight");
        if (Input.GetKeyDown(DashKey) && Input.GetKey(LeftKey) && !anim.GetBool("Dash"))
            StartCoroutine("DashLeft");
        if (doubletapR)
        {
            rb2d.AddForce(Vector2.right * speed * 4);
            if (grounded && hit.collider == null)
            {
                StopCoroutine("DashRight");
                Debug.Log("stoooop");
                StartCoroutine("StopDash");
            }

        }
        if (doubletapL)
        {
            rb2d.AddForce(Vector2.right * speed * -4);
            if (grounded && hit.collider == null)
            {
                StopCoroutine("DashLeft");
                Debug.Log("stoooop");
                StartCoroutine("StopDash");
            }
        }
        }


    public IEnumerator PreJump()
    {
        anim.SetBool("JumpCheck", true);
        sounds.SendMessageUpwards("sound11");
        yield return new WaitForSeconds(.1f);
        rb2d.AddForce(Vector2.up * jumpPower);
    }
    public IEnumerator AttackStop()
    {
        attack.enabled = false;
        yield return new WaitForSeconds(.1f);
        attack.enabled = true;
    }
    public IEnumerator DoubleJump()
    {
        doubleJump = true;
        rb2d.velocity = Vector3.zero;
        yield return new WaitForSeconds(.05f);
        rb2d.AddForce(Vector2.up * jumpPower*1.15f);
    }

    public void Knockback(int direction)
    {
        StartCoroutine("Knock", direction);
    }
    public IEnumerator Knock(int direction)
    {
        rb2d.velocity = Vector3.zero;
        yield return new WaitForSeconds(.05f);
        if (direction == 1)
            rb2d.AddForce(new Vector3(-220, 300, transform.position.z));
        else if (direction == 2)
            rb2d.AddForce(new Vector3(220, 300, transform.position.z));
        //back
        else if (direction == 3)
            rb2d.AddForce(new Vector3(-220, 120, transform.position.z));
        else if (direction == 4)
            rb2d.AddForce(new Vector3(220, 120, transform.position.z));
        //weak
        else if (direction == 5)
            rb2d.AddForce(new Vector3(-120, 120, transform.position.z));
        else if (direction == 6)
            rb2d.AddForce(new Vector3(120, 120, transform.position.z));
        //up
        else if (direction == 7)
            rb2d.AddForce(new Vector3(-60, 550, transform.position.z));
        else if (direction == 8)
            rb2d.AddForce(new Vector3(60, 550, transform.position.z));
    }

    public IEnumerator DashRight()
    {
        maxSpeed = 0;
        yield return new WaitForSeconds(.12f);
        anim.SetBool("Dash", true);
        sounds.SendMessageUpwards("sound11");
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player1").GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Player2").GetComponent<BoxCollider2D>(),true);
        attack.enabled = false;
        doubletapR = true;
        maxSpeed = 10;
        rb2d.AddForce(new Vector2(0, 10));
        yield return new WaitForSeconds(.22f);
        rb2d.velocity = Vector3.zero;
        maxSpeed = 0;
        doubletapR = false;
        anim.SetBool("Dash", false);
        attack.enabled = true;
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player1").GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Player2").GetComponent<BoxCollider2D>(), false);
        yield return new WaitForSeconds(.2f);
        maxSpeed = 2.5f;
    }
    public IEnumerator DashLeft()
    {
        maxSpeed = 0;
        yield return new WaitForSeconds(.12f);
        anim.SetBool("Dash", true);
        sounds.SendMessageUpwards("sound11");
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player1").GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Player2").GetComponent<BoxCollider2D>(), true);
        attack.enabled = false;
        doubletapL = true;
        maxSpeed = 10;
        rb2d.AddForce(new Vector2(0, 10));
        yield return new WaitForSeconds(.22f);
        rb2d.velocity = Vector3.zero;
        maxSpeed = 0;
        doubletapL = false;
        anim.SetBool("Dash", false);
        attack.enabled = true;
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player1").GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Player2").GetComponent<BoxCollider2D>(), false);
        yield return new WaitForSeconds(.2f);
        maxSpeed = 2.5f;

    }
    public IEnumerator StopDash()
    {
        rb2d.velocity = Vector3.zero;
        maxSpeed = 0;
        doubletapL = false;
        doubletapR = false;
        anim.SetBool("Dash", false);
        attack.enabled = true;
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player1").GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Player2").GetComponent<BoxCollider2D>(), false);
        yield return new WaitForSeconds(.2f);
        maxSpeed = 2.5f;

    }

}
