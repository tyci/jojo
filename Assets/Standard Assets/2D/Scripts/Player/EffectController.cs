﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    private Animator anim;
    public int blood;
    public bool defend;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        anim.SetInteger("Blood", blood);
        anim.SetBool("Defend", defend);
    }
}
