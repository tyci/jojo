﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
    public AudioSource audio;
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
    public AudioClip audio1;
    public AudioClip audio2;
    public AudioClip audio3;
    public AudioClip audio4;
    public AudioClip audio5;
    public AudioClip audio6;
    public AudioClip audio7;
    public AudioClip audio8;
    public AudioClip audio9;
    public AudioClip audio10;
    public AudioClip audio11;
    public AudioClip audio12;
    public AudioClip audio13;
    public AudioClip audio14;
    public AudioClip audio15;
    public AudioClip audio16;
    public AudioClip audio17;
    public AudioClip audio18;
    public AudioClip audio20;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    public void sound1()
    {
        audio.PlayOneShot(audio1, 0.6f);
    }
    public void sound2()
    {
        audio.PlayOneShot(audio2, 0.6f);
    }
    public void sound3()
    {
        audio.PlayOneShot(audio3, 0.8f);
    }
    public void sound4()
    {
        audio.PlayOneShot(audio4, .7f);
    }
    public void sound5()
    {
        audio.PlayOneShot(audio5, 0.7f);
    }
    public void sound6()
    {
        audio.PlayOneShot(audio6, 0.7f);
    }
    public void sound7()
    {
        audio.PlayOneShot(audio7, 0.7f);
    }
    public void sound8()
    {
        audio.PlayOneShot(audio8, 0.7f);
    }
    public void sound9()
    {
        audio.PlayOneShot(audio9, 0.7f);
    }
    public void sound10()
    {
        audio.PlayOneShot(audio10, 0.7f);
    }
    public void sound11()
    {
        audio.PlayOneShot(audio11, 0.7f);
    }
    public void sound12()
    {
        audio.PlayOneShot(audio12, 0.7f);
    }
    public void sound13()
    {
        audio.PlayOneShot(audio13, 0.7f);
    }
    public void sound14()
    {
        audio.PlayOneShot(audio14, 0.7f);
    }
    public void sound15()
    {
        audio.PlayOneShot(audio15, 0.7f);
    }
    public void sound16()
    {
        audio.PlayOneShot(audio16, 0.7f);
    }
    public void sound17()
    {
        audio.PlayOneShot(audio17, 0.7f);
    }
    public void sound18()
    {
        audio.PlayOneShot(audio18, 0.7f);
    }

    public void sound20()
    {
        audio.loop = true;
        audio.clip = audio20;
        audio.Play();
    }

    public void random(int i)
    {
        switch (i)
        {
            case 1:
                audio.PlayOneShot(audio1, 0.8f);
                break;
            case 2:
                audio.PlayOneShot(audio2, 0.8f);
                break;
            case 3:
                audio.PlayOneShot(audio3, 0.8f);
                break;
            case 4:
                audio.PlayOneShot(audio4, 0.8f);
                break;
            case 5:
                audio.PlayOneShot(audio5, 0.8f);
                break;
            case 6:
                audio.PlayOneShot(audio6, 0.8f);
                break;
            case 7:
                audio.PlayOneShot(audio7, 0.8f);
                break;
            case 8:
                audio.PlayOneShot(audio8, 0.8f);
                break;
            case 9:
                audio.PlayOneShot(audio9, 0.8f);
                break;
            case 10:
                audio.PlayOneShot(audio10, 0.8f);
                break;
            case 11:
                audio.PlayOneShot(audio11, 0.8f);
                break;
            case 12:
                audio.PlayOneShot(audio12, 0.8f);
                break;
            case 13:
                audio.PlayOneShot(audio13, 0.8f);
                break;
            case 14:
                audio.PlayOneShot(audio14, 0.8f);
                break;
            case 15:
                audio.PlayOneShot(audio15, 0.8f);
                break;
        }

        }
    public void stop()
    {
        audio.Stop();
        audio.loop = false;
    }
}
