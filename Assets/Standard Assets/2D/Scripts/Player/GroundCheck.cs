﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    private PlayerControl player;
    private Rigidbody2D rb2d;
    public Collider2D attackTrigger;
    public KeyCode DownKey;
    public KeyCode JumpKey;

    private void Start()
    {
        player = gameObject.GetComponentInParent<PlayerControl>();
        rb2d = gameObject.GetComponentInParent<Rigidbody2D>();
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Ground"))
        {
            player.grounded = true;
        }


    }
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Ground"))
        {
            player.grounded = true;
        }

        if (col.CompareTag("Platform") && rb2d.velocity.y < 0.1f)
        {
            player.grounded = true;
            if (Input.GetKey(DownKey)&& Input.GetKeyDown(JumpKey))
            {
                StartCoroutine("Drop");
            }
        }

        if (col.CompareTag("Head"))
        {
            rb2d.GetComponent<BoxCollider2D>().enabled = false;
            if (player.transform.position.x > attackTrigger.transform.position.x)
                rb2d.AddForce(new Vector3(-50, 0, rb2d.transform.position.z));
            else
                rb2d.AddForce(new Vector3(50, 0, rb2d.transform.position.z));
        }

        

    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Ground"))
        {
            player.grounded = false;
            
        }
        if (col.CompareTag("Platform"))
        {
            player.grounded = false;

        }

        if (col.CompareTag("Head"))
        {
            rb2d.GetComponent<BoxCollider2D>().enabled = true;
        }
        
    }


    public IEnumerator Drop()
    {
        rb2d.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(.2f);
        rb2d.GetComponent<BoxCollider2D>().enabled = true;

    }
}

