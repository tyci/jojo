﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandPower : MonoBehaviour
{
    private Animator anim;
    public bool IsOn = false;
    public bool charging = false;
    public bool talking = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        anim.SetBool("IsOn", IsOn);
        anim.SetBool("Charging", charging);
        anim.SetBool("talk", talking);
    }
}
