﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{
    protected PlayersAttack plr;

    protected CameraShake camShake;
    protected BoxCollider2D attack;
    protected Sounds sounds;

    protected virtual void Start()
    {
        plr = gameObject.GetComponentInParent<PlayersAttack>();
        camShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
        attack = GetComponent<BoxCollider2D>();
        sounds = GameObject.FindGameObjectWithTag("Sound").GetComponent<Sounds>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {

        //basic attack
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && col.attachedRigidbody.gravityScale != 3.1f
            && !plr.combo && !plr.jumpAttack && !plr.uppercut && !plr.frontattack && !plr.kick )
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 1);
            else if (col.attachedRigidbody.mass == 1.5f)
            {     
                col.SendMessageUpwards("Defence");      //block
            }

            attack.size = new Vector2(0, 0);
            int rInt = Random.Range(1, 6);
            sounds.SendMessageUpwards("random",rInt);
            Debug.Log("dmg");
            camShake.Shake(0.1f, 0.1f);
        }
        //knockback attack
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.combo && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 2);
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Damage", 1);    //block
                col.SendMessageUpwards("Defence");
            }
                


            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 1);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 2);
            }


            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            Debug.Log("dmg2");
            camShake.Shake(0.2f, 0.1f);
        }
        //jump attack
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.jumpAttack && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }
            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 3);
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Defence");      //block
            }

            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 5);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 6);
            }

            int rInt = Random.Range(1, 6);
            sounds.SendMessageUpwards("random", rInt);
            Debug.Log("dmg3");
            camShake.Shake(0.2f, 0.1f);
        }

        //uppercut
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.uppercut && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 1);
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Defence");      //block
            }


            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 7);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 8);
            }

            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            attack.size = new Vector2(0, 0);
            camShake.Shake(0.1f, 0.2f);
        }

        //front attack
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.frontattack && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
                col.SendMessageUpwards("Damage", 1);
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Defence");      //block
            }


            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 3);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 4);
            }

            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            attack.size = new Vector2(0, 0);
            camShake.Shake(0.1f, 0.2f);
        }
        //kick
        if (col.isTrigger != true && !col.CompareTag("Ground") && !col.CompareTag("Platform") && plr.kick && col.attachedRigidbody.gravityScale != 3.1f)
        {
            if (col.attachedRigidbody.mass == 2f)    //counter check
            {
                col.SendMessageUpwards("Counter");
            }

            if (col.attachedRigidbody.mass == 1f)    //not block
            {
                col.SendMessageUpwards("Damage", 5);
                col.SendMessageUpwards("Damage", 1);
            }
            else if (col.attachedRigidbody.mass == 1.5f)
            {
                col.SendMessageUpwards("Defence");      //block
            }

            if (col.transform.position.x < plr.transform.position.x)
            {
                col.SendMessageUpwards("Knockback", 5);
            }
            else
            {
                col.SendMessageUpwards("Knockback", 6);
            }

            int rInt = Random.Range(6, 10);
            sounds.SendMessageUpwards("random", rInt);
            camShake.Shake(0.1f, 0.2f);
        }

    }


}