﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public AudioSource audiosor;
    public AudioClip music1;
    public AudioClip music2;
    public AudioClip music3;
    private void Start()
    {
        audiosor = GetComponent<AudioSource>();
        if (GameData.music == 1)
            audiosor.PlayOneShot(music1);
        else if (GameData.music == 2)
            audiosor.PlayOneShot(music2);
       else  if (GameData.music == 3)
            audiosor.PlayOneShot(music3);
    }


}
